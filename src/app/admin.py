from django.contrib import admin

from .internal.bank_accounts.presentation.admin import BankAccountAdmin, BankCurrencyAdmin, BankExchangeRateAdmin
from .internal.bank_cards.presentation.admin import BankCardAdmin
from .internal.bank_transactions.presentation.admin import BankTransactionAdmin
from .internal.favourite_users_lists.presentation.admin import FavouriteAccountsListAdmin
from .internal.users.presentation.admin import TelegramBotUserAdmin

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
