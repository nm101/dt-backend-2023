from django.core.management.base import BaseCommand

from app.internal.bot import TelegramBot
from config import settings


class Command(BaseCommand):
    help = "Start Telegram bot in polling mode"

    def handle(self, *args, **options):
        print("Starting Telegram bot in polling mode..")
        token = settings.TELEGRAM_BOT_TOKEN

        bot = TelegramBot(token)
        bot.start_polling()
