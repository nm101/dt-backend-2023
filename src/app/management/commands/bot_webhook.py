from django.core.management.base import BaseCommand

from app.internal.bot import TelegramBot
from config import settings


class Command(BaseCommand):
    help = "Start Telegram bot in webhook mode"

    def add_arguments(self, parser):
        parser.add_argument("host", type=str, default="127.0.0.1")
        parser.add_argument("port", type=int, default=8000)

    def handle(self, *args, **options):
        print(
            "Starting Telegram bot in webhook mode..\n"
            + f"Listening on {options['host']}:{options['port']}/telegram\n"
            + f"Remote webhook URL set to {settings.TELEGRAM_BOT_WEBHOOK_URL}"
        )
        token = settings.TELEGRAM_BOT_TOKEN

        bot = TelegramBot(token)
        bot.start_webhook(options["host"], options["port"], settings.TELEGRAM_BOT_WEBHOOK_URL)
