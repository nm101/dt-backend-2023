from django.urls import path

from app.internal import app

urlpatterns = [
    path("", app.ninja_api.urls),
]
