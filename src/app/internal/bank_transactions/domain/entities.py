from __future__ import annotations

import traceback
from dataclasses import dataclass
from datetime import datetime
from decimal import Decimal
from enum import IntEnum
from typing import Optional, Union

from botocore.exceptions import ClientError
from django.conf import settings
from django.db.models.fields.files import ImageFieldFile

from app.internal.bank_accounts.domain.entities import BankAccountInfo
from app.internal.bank_cards.domain.entities import BankCardInfo
from app.internal.bank_transactions.db.models import BankTransaction
from app.internal.bank_transactions.domain.payment_type import PaymentType
from app.internal.users.domain.entities import TelegramBotUserInfo


@dataclass
class BankTransactionRequisitesInfo:
    user: TelegramBotUserInfo
    payment_type: PaymentType
    bank_account: Optional[BankAccountInfo]
    bank_card: Optional[BankCardInfo]


@dataclass
class PostcardInfo:
    signed_url: str
    content: bytes

    @staticmethod
    def from_orm(field: ImageFieldFile) -> Optional[PostcardInfo]:
        if not field:
            return None

        try:
            url = field.storage.bucket.meta.client.generate_presigned_url(
                "get_object", Params={"Bucket": settings.AWS_STORAGE_BUCKET_NAME, "Key": field.name}
            )

            with field.open("rb") as f:
                content = f.read()

            return PostcardInfo(url, content)
        except FileNotFoundError | ClientError:
            traceback.print_exc()
            return None


@dataclass
class BankTransactionInfo:
    is_committed: bool
    timestamp: datetime
    source_amount: Decimal

    source: BankTransactionRequisitesInfo
    destination: BankTransactionRequisitesInfo

    postcard: Optional[PostcardInfo]

    @staticmethod
    def from_orm(entity: BankTransaction) -> BankTransactionInfo:
        source_requisites = BankTransactionRequisitesInfo(
            user=TelegramBotUserInfo.from_orm(entity.source_user),
            payment_type=PaymentType(entity.source_type),
            bank_card=BankCardInfo.from_orm(entity.source_card) if entity.source_type == PaymentType.BankCard else None,
            bank_account=BankAccountInfo.from_orm(
                entity.source_account if entity.source_type == PaymentType.BankAccount else entity.source_card.account
            ),
        )
        destination_requisites = BankTransactionRequisitesInfo(
            user=TelegramBotUserInfo.from_orm(entity.destination_user),
            payment_type=PaymentType(entity.destination_type),
            bank_card=BankCardInfo.from_orm(entity.destination_card)
            if entity.destination_type == PaymentType.BankCard
            else None,
            bank_account=BankAccountInfo.from_orm(
                entity.destination_account
                if entity.destination_type == PaymentType.BankAccount
                else entity.destination_card.account
            ),
        )

        return BankTransactionInfo(
            entity.is_committed,
            entity.timestamp,
            entity.amount,
            source_requisites,
            destination_requisites,
            PostcardInfo.from_orm(entity.postcard),
        )


class BankAccountStatementKind(IntEnum):
    Income = 0
    Expense = 1


@dataclass
class BankAccountStatementInfo:
    timestamp: datetime
    kind: BankAccountStatementKind
    source_amount: Decimal
    related_user: TelegramBotUserInfo
    related_requisites: Union[BankAccountInfo, BankCardInfo]
    postcard: Optional[PostcardInfo]

    @staticmethod
    def from_orm(
        transaction: BankTransactionInfo, target_user_requisites: Union[BankAccountInfo, BankCardInfo]
    ) -> BankAccountStatementInfo:
        kind = BankAccountStatementKind.Expense
        related_transaction_requisites = transaction.destination
        if (
            isinstance(target_user_requisites, BankAccountInfo)
            and transaction.destination.payment_type == PaymentType.BankAccount
            and transaction.destination.bank_account.id == target_user_requisites.id
        ) or (
            isinstance(target_user_requisites, BankCardInfo)
            and transaction.destination.payment_type == PaymentType.BankCard
            and transaction.destination.bank_card.number == target_user_requisites.number
        ):
            related_transaction_requisites = transaction.source
            kind = BankAccountStatementKind.Income

        related_user = related_transaction_requisites.user
        match related_transaction_requisites.payment_type:
            case PaymentType.BankAccount:
                related_requisites = related_transaction_requisites.bank_account
            case PaymentType.BankCard:
                related_requisites = related_transaction_requisites.bank_card
            case _:
                raise ValueError("Unknown related requisites PaymentType")

        return BankAccountStatementInfo(
            transaction.timestamp,
            kind,
            transaction.source_amount,
            related_user,
            related_requisites,
            transaction.postcard,
        )
