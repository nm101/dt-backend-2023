from enum import IntEnum


class PaymentType(IntEnum):
    BankAccount = 0
    BankCard = 1
