class InsufficientMoney(Exception):
    pass


class TransactionReCommitOrRollbackDetected(Exception):
    pass
