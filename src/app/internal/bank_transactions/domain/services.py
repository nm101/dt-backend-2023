import logging
from datetime import timedelta
from decimal import Decimal
from io import BytesIO
from typing import Set, Tuple

from asgiref.sync import sync_to_async
from django.core.exceptions import ValidationError
from django.core.files.images import ImageFile
from django.db import transaction as orm_transaction
from django.db.models import DateTimeField, F, Q, QuerySet, Sum
from django.db.models.functions import Cast
from django.utils import timezone

from app.internal.bank_accounts.db.models import BankAccount
from app.internal.bank_accounts.domain.entities import BankAccountInfo
from app.internal.bank_accounts.domain.exceptions import CurrencyConversionImpossible
from app.internal.bank_accounts.domain.services import BankExchangeRateService
from app.internal.bank_cards.domain.entities import BankCardInfo
from app.internal.bank_transactions.db.models import BankTransaction
from app.internal.bank_transactions.domain.entities import BankAccountStatementInfo, BankTransactionInfo
from app.internal.bank_transactions.domain.exceptions import InsufficientMoney, TransactionReCommitOrRollbackDetected
from app.internal.bank_transactions.domain.payment_type import PaymentType
from app.internal.users.db.models import TelegramBotUser
from app.internal.users.domain.entities import TelegramBotUserInfo


class BankTransactionService:
    def __init__(self, exchange_rate_service: BankExchangeRateService):
        self._exchange_rate_service = exchange_rate_service
        self._logger = logging.getLogger(__name__)

    def get_transaction_sum_for_last_10_minutes(self) -> Decimal:
        least_timestamp = timezone.now() - timedelta(minutes=10)
        aggregation = BankTransaction.objects.filter(
            Q(timestamp__gte=Cast(least_timestamp, DateTimeField())) & Q(is_committed=True)
        ).aggregate(sum=Sum("amount"))
        result = aggregation["sum"]

        if result is None:
            return Decimal("0")
        return result

    def is_transfer_between_accounts_possible(
        self, source: BankAccount, destination: BankAccount, amount: Decimal
    ) -> bool:
        try:
            self._exchange_rate_service.get(source.currency, destination.currency)
        except CurrencyConversionImpossible:
            return False
        return source.balance >= amount

    @sync_to_async
    def ais_transfer_between_accounts_possible(
        self, source: BankAccount, destination: BankAccount, amount: Decimal
    ) -> bool:
        return self.is_transfer_between_accounts_possible(source, destination, amount)

    def validate(self, transaction: BankTransaction) -> None:
        if transaction.source_account is None or transaction.destination_account is None:
            raise ValidationError("Account fields mustn't be null")

        if transaction.source_type == PaymentType.BankCard and transaction.source_card is None:
            raise ValidationError("When the source type set to card, source card field mustn't be null")

        if transaction.destination_type == PaymentType.BankCard and transaction.destination_card is None:
            raise ValidationError("When the destination type set to card, destination card field mustn't be null")

        source_payment_type_user = (
            transaction.source_card.owner
            if transaction.source_type == PaymentType.BankCard
            else transaction.source_account.owner
        )

        destination_payment_type_user = (
            transaction.destination_card.owner
            if transaction.destination_type == PaymentType.BankCard
            else transaction.destination_account.owner
        )

        if transaction.source_user != source_payment_type_user:
            raise ValidationError("Source user doesn't match payment type's one")

        if transaction.destination_user and transaction.destination_user != destination_payment_type_user:
            raise ValidationError("Destination user doesn't match payment type's one")

        if (
            transaction.source_type == PaymentType.BankCard
            and transaction.source_card.account != transaction.source_account
        ):
            raise ValidationError("Source card account doesn't match source account")

        if (
            transaction.destination_type == PaymentType.BankCard
            and transaction.destination_card.account != transaction.destination_account
        ):
            raise ValidationError("Destination card account doesn't match source account")

    def is_valid(self, transaction: BankTransaction) -> bool:
        try:
            self.validate(transaction)
            return True
        except ValidationError:
            return False

    def is_applicable(self, transaction: BankTransaction) -> bool:
        if not self.is_valid(transaction) or transaction.is_committed:
            return False

        return self.is_transfer_between_accounts_possible(
            transaction.source_account, transaction.destination_account, transaction.amount
        )

    def _get_destination_balance_increment(self, transaction: BankTransaction) -> Decimal:
        rate = self._exchange_rate_service.get(
            transaction.source_account.currency, transaction.destination_account.currency
        )
        return transaction.amount / rate

    def commit(self, transaction: BankTransaction) -> None:
        try:
            self.validate(transaction)

            with orm_transaction.atomic():
                if transaction.is_committed:
                    raise TransactionReCommitOrRollbackDetected("Attempted to re-commit a committed transaction")

                destination_increment = self._get_destination_balance_increment(transaction)

                BankAccount.objects.filter(
                    Q(id=transaction.source_account.id) | Q(id=transaction.destination_account.id)
                ).only("balance").select_for_update()

                transaction.source_account.refresh_from_db()
                transaction.destination_account.refresh_from_db()

                if transaction.source_account.balance < transaction.amount:
                    raise InsufficientMoney()

                transaction.source_account.balance = F("balance") - transaction.amount
                transaction.source_account.save()

                transaction.destination_account.balance = F("balance") + destination_increment
                transaction.destination_account.save()

                transaction.is_committed = True
                transaction.save()

                self._logger.info(
                    f"Transaction commit succeeded: ID = {transaction.id}, "
                    f"Source User = {transaction.source_user}, "
                    f"Destination User = {transaction.destination_user}"
                )
        except BaseException as e:
            reason = "Unknown"
            if isinstance(e, InsufficientMoney):
                reason = "Source account insufficient money"
            if isinstance(e, ValidationError):
                reason = f"Transaction validation failed: {e.message}"

            self._logger.info(
                f"Transaction commit failed: ID = {transaction.id}, "
                f"Source User = {transaction.source_user}, "
                f"Destination User = {transaction.destination_user}, "
                f"Reason = {reason}"
            )
            raise e

    @sync_to_async
    def acommit(self, transaction: BankTransaction) -> None:
        return self.commit(transaction)

    def rollback(self, transaction: BankTransaction) -> None:
        self.validate(transaction)

        with orm_transaction.atomic():
            transaction.refresh_from_db()
            if not transaction.is_committed:
                raise TransactionReCommitOrRollbackDetected("Attempted to rollback non-committed transaction")

            destination_decrement = self._get_destination_balance_increment(transaction)

            BankAccount.objects.filter(
                Q(id=transaction.source_account.id) | Q(id=transaction.destination_account.id)
            ).only("balance").select_for_update()

            transaction.source_account.balance = F("balance") + transaction.amount
            transaction.source_account.save()

            transaction.destination_account.balance = F("balance") - destination_decrement
            transaction.destination_account.save()

            transaction.is_committed = False
            transaction.save()

    @sync_to_async
    def arollback(self, transaction: BankTransaction) -> None:
        return self.rollback(transaction)

    def create_blank(self, source_user_id: int) -> BankTransaction:
        return BankTransaction(source_user_id=source_user_id)

    def set_destination_bank_account(self, transaction: BankTransaction, account: BankAccountInfo) -> None:
        transaction.destination_type = PaymentType.BankAccount
        transaction.destination_account_id = account.id
        transaction.destination_user_id = account.owner_id

    @sync_to_async
    def aset_destination_bank_account(self, transaction: BankTransaction, account: BankAccountInfo) -> None:
        return self.set_destination_bank_account(transaction, account)

    def set_destination_bank_card(self, transaction: BankTransaction, card: BankCardInfo) -> None:
        transaction.destination_type = PaymentType.BankCard
        transaction.destination_card_id = card.number
        transaction.destination_account_id = card.account_id
        transaction.destination_user_id = card.owner_id

    @sync_to_async
    def aset_destination_bank_card(self, transaction: BankTransaction, card: BankCardInfo) -> None:
        return self.set_destination_bank_card(transaction, card)

    def set_destination_user(self, transaction: BankTransaction, user_id: int) -> None:
        transaction.destination_user_id = user_id

    @sync_to_async
    def aset_destination_user(self, transaction: BankTransaction, user_id: int) -> None:
        return self.set_destination_user(transaction, user_id)

    def set_source_bank_account(self, transaction: BankTransaction, account_id: str) -> None:
        transaction.source_type = PaymentType.BankAccount
        transaction.source_account_id = account_id

    @sync_to_async
    def aset_source_bank_account(self, transaction: BankTransaction, account: BankAccountInfo) -> None:
        return self.set_source_bank_account(transaction, account.id)

    def set_source_bank_card(self, transaction: BankTransaction, card: BankCardInfo) -> None:
        transaction.source_type = PaymentType.BankCard
        transaction.source_card_id = card.number
        transaction.source_account_id = card.account_id

    @sync_to_async
    def aset_source_bank_card(self, transaction: BankTransaction, card: BankCardInfo) -> None:
        return self.set_source_bank_card(transaction, card)

    def set_amount(self, transaction: BankTransaction, amount: Decimal) -> None:
        if not amount or amount <= 0 or amount.as_tuple().exponent < -2:
            raise ValueError("Incorrect amount")

        transaction.amount = amount

    @sync_to_async()
    def aset_amount(self, transaction: BankTransaction, amount: Decimal) -> None:
        return self.set_amount(transaction, amount)

    def get_related_with_user(self, telegram_id: int) -> QuerySet[BankTransaction]:
        return BankTransaction.objects.filter(
            Q(is_committed=True) & (Q(source_user_id=telegram_id) | Q(destination_user_id=telegram_id))
        )

    def get_related_with_bank_account_by_account_statement_period(self, account_id: str) -> QuerySet[BankTransaction]:
        return BankTransaction.by_account_statement_period_objects.filter(
            (Q(source_type=PaymentType.BankAccount) & Q(source_account__id=account_id))
            | (Q(destination_type=PaymentType.BankAccount) & Q(destination_account__id=account_id))
        )

    def get_related_with_bank_card_by_account_statement_period(self, card_number: int) -> QuerySet[BankTransaction]:
        return BankTransaction.by_account_statement_period_objects.filter(
            (Q(source_type=PaymentType.BankCard) & Q(source_card__number=card_number))
            | (Q(destination_type=PaymentType.BankCard) & Q(destination_card__number=card_number))
        )

    def next_not_seen_inbound_transactions_for_user(self, telegram_id: int) -> Tuple[BankTransactionInfo]:
        ids = tuple(
            item[0]
            for item in BankTransaction.objects.filter(
                Q(destination_user_id=telegram_id) & Q(is_seen=False)
            ).values_list("id")
        )

        BankTransaction.objects.filter(id__in=ids).update(is_seen=True)

        queryset = (
            BankTransaction.objects.filter(id__in=ids)
            .order_by("-timestamp")
            .select_related(
                "source_user",
                "destination_user",
                "source_card",
                "source_card__account",
                "source_card__account__currency",
                "destination_card",
                "destination_card__account",
                "destination_card__account__currency",
                "source_account",
                "source_account__currency",
                "destination_account",
                "destination_account__currency",
            )
        )
        return tuple(BankTransactionInfo.from_orm(item) for item in queryset)

    @sync_to_async
    def anext_not_seen_inbound_transactions_for_user(self, telegram_id: int) -> Tuple[BankTransactionInfo]:
        return self.next_not_seen_inbound_transactions_for_user(telegram_id)

    def set_postcard(self, transaction: BankTransaction, name: str, content: bytes):
        image_file = ImageFile(BytesIO(content), name=f"{name}.png")
        transaction.postcard = image_file


class BankStatisticsService:
    def __init__(self, transaction_service: BankTransactionService):
        self._transaction_service = transaction_service

    def get_related_users_info(self, telegram_id: int) -> Set[TelegramBotUserInfo]:
        queryset = (
            self._transaction_service.get_related_with_user(telegram_id)
            .select_related("source_user", "destination_user")
            .only("source_user", "destination_user")
        )

        def selector(transaction: BankTransaction) -> TelegramBotUser:
            match telegram_id:
                case transaction.source_user.telegram_id:
                    return transaction.destination_user
                case transaction.destination_user.telegram_id:
                    return transaction.source_user
                case _:
                    raise ValueError("Invalid transaction submitted")

        return set(TelegramBotUserInfo.from_orm(selector(transaction)) for transaction in queryset.all())

    @sync_to_async
    def aget_related_users_info(self, telegram_id: int) -> Set[TelegramBotUserInfo]:
        return self.get_related_users_info(telegram_id)

    def _process_queryset_for_account_statement(self, queryset: QuerySet[BankTransaction]) -> QuerySet[BankTransaction]:
        return queryset.order_by("-timestamp").select_related(
            "source_user",
            "destination_user",
            "source_card",
            "source_card__account",
            "source_card__account__currency",
            "destination_card",
            "destination_card__account",
            "destination_card__account__currency",
            "source_account",
            "source_account__currency",
            "destination_account",
            "destination_account__currency",
        )

    def get_account_statement_for_bank_card(self, card: BankCardInfo) -> Tuple[BankAccountStatementInfo, ...]:
        queryset = self._process_queryset_for_account_statement(
            self._transaction_service.get_related_with_bank_card_by_account_statement_period(card.number)
        )

        transaction_infos = (BankTransactionInfo.from_orm(entity) for entity in queryset.all())
        return tuple(BankAccountStatementInfo.from_orm(entity, card) for entity in transaction_infos)

    @sync_to_async()
    def aget_account_statement_for_bank_card(self, card: BankCardInfo) -> Tuple[BankAccountStatementInfo, ...]:
        return self.get_account_statement_for_bank_card(card)

    def get_account_statement_for_bank_account(self, account: BankAccountInfo) -> Tuple[BankAccountStatementInfo, ...]:
        queryset = self._process_queryset_for_account_statement(
            self._transaction_service.get_related_with_bank_account_by_account_statement_period(account.id)
        )

        transaction_infos = (BankTransactionInfo.from_orm(entity) for entity in queryset.all())
        return tuple(BankAccountStatementInfo.from_orm(entity, account) for entity in transaction_infos)

    @sync_to_async
    def aget_account_statement_for_bank_account(self, account: BankAccountInfo) -> Tuple[BankAccountStatementInfo, ...]:
        return self.get_account_statement_for_bank_account(account)
