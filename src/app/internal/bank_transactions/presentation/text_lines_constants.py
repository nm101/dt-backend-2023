from typing import Set, Tuple, Union

from django.core.exceptions import ValidationError

from app.internal.bank_accounts.domain.entities import BankAccountInfo
from app.internal.bank_accounts.domain.exceptions import CurrencyConversionImpossible
from app.internal.bank_cards.domain.entities import BankCardInfo
from app.internal.bank_transactions.domain.entities import (
    BankAccountStatementInfo,
    BankAccountStatementKind,
    BankTransactionInfo,
)
from app.internal.bank_transactions.domain.exceptions import InsufficientMoney
from app.internal.bank_transactions.domain.payment_type import PaymentType
from app.internal.common.presentation.text_lines_constants import (
    general_notice_cancel_command,
    general_reply_only_with_digit_choice,
)
from app.internal.users.domain.entities import TelegramBotUserInfo

state_transfer_source_type_card_list_title = "Please choose a card from list below:"
state_transfer_source_type_account_list_title = "Please choose an account from list below:"
command_transfer_specify_destination_type = (
    "Please choose a destination:\n"
    "1) To a Telegram user from favourites\n"
    "2) To other telegram user\n"
    "3) To a particular bank card\n"
    "4) To a particular bank account\n"
    "\n" + general_reply_only_with_digit_choice + "\n" + general_notice_cancel_command
)
state_transfer_general_choose_source_type = (
    "Please choose a source:\n"
    "1) From a bank card\n"
    "2) From a bank account\n"
    "\n" + general_reply_only_with_digit_choice
)
state_transfer_general_bank_account_not_found = "Bank account not found, please try again"
state_transfer_awaiting_source_choice_amount_question = "Please specify amount to transfer:"
state_transfer_awaiting_amount_incorrect_message = (
    "Specified amount is incorrect.\n"
    "Transfer amount must be a positive number with no more than 2 decimal places "
    "and have only a single comma or point, dividing integral and fractional parts"
)
state_transaction_confirmation_request = (
    "All information is saved. Are you sure want to commit transaction?\n" "1) Yes\n" "2) No"
)
state_transfer_awaiting_confirmation_user_decline_message = "Operation cancelled"
state_transfer_awaiting_confirmation_transaction_success_message = "Transaction successfully applied"


def get_state_transfer_awaiting_confirmation_transaction_fail_message(exception: Exception) -> str:
    reason = "Unknown"
    if isinstance(exception, ValidationError):
        reason = "Internal failure"

    if isinstance(exception, InsufficientMoney):
        reason = "Insufficient money on a payment source"

    if isinstance(exception, CurrencyConversionImpossible):
        reason = f"Currency conversion impossible: {exception}"

    return f"Transaction failed. Reason: {reason}\n" "Do you want to try again? Type 1 for Yes, 2 for No"


state_awaiting_destination_type_to_bank_account_message = "Please specify bank account ID"
state_awaiting_destination_type_to_bank_card_message = "Please specify bank card number"
state_to_bank_card_card_number_invalid_message = "Invalid card number submitted, please try again"
state_to_bank_card_card_number_does_not_exist_message = "A card with specified number does not exist"
general_user_destination_type_text = (
    "Please choose destination payment type number:\n" "1) Bank card\n" "2) Bank account"
)
transfer_to_user_cards_list_title = "Please choose card number in list:"
transfer_to_user_accounts_list_title = "Please choose account number in list:"
awaiting_destination_type_to_fave_user_title = "Please choose user number from favourites:"
command_related_users_header = "According to your transactions, you've been interacting with these users:"


def command_related_users_list_form(users: Set[TelegramBotUserInfo]):
    lines = tuple(f"@{user.username}" for user in users)

    return "\n".join((command_related_users_header, "") + lines)


command_account_statement_text = (
    "Please choose account statement source:\n\n"
    "1) Bank card\n"
    "2) Bank account\n\n" + general_reply_only_with_digit_choice
)
state_account_statement_awaiting_source_type_card_choice_header = (
    "Please choose a card you wish to get account statement for:"
)
state_account_statement_awaiting_source_type_account_choice_header = (
    "Please choose an account you wish to get account statement for:"
)
DATETIME_DISPLAY_FORMAT = "%a %b %d %H:%M:%S %Y"


def general_account_statement_info_format(
    statement: BankAccountStatementInfo,
    target_user_requisites: Union[BankCardInfo, BankAccountInfo],
    display_signed_url: bool = True,
) -> str:
    timestamp = statement.timestamp.strftime(DATETIME_DISPLAY_FORMAT)
    related_requisites = (
        f"bank card {statement.related_requisites.number_displayable_public}"
        if isinstance(statement.related_requisites, BankCardInfo)
        else f"bank account {statement.related_requisites.id}"
    )

    postcard_text = (
        f"\nPostcard signed URL: {statement.postcard.signed_url}" if statement.postcard and display_signed_url else ""
    )

    match statement.kind:
        case BankAccountStatementKind.Income:
            currency_str = statement.related_requisites.currency_str
            return (
                f"{timestamp}: received {statement.source_amount} {currency_str}\n"
                f"  from @{statement.related_user.username}\n"
                f"  via {related_requisites}" + postcard_text
            )

        case BankAccountStatementKind.Expense:
            return (
                f"{timestamp}: sent {statement.source_amount} {target_user_requisites.currency_str}\n"
                f"  to @{statement.related_user.username}\n"
                f"  via {related_requisites}" + postcard_text
            )


def general_account_statements_format(
    statements: Tuple[BankAccountStatementInfo, ...], target_user_requisites: Union[BankAccountInfo, BankCardInfo]
) -> str:
    items = tuple(general_account_statement_info_format(statement, target_user_requisites) for statement in statements)

    return "\n\n".join(items)


text_prompt_request_attach_postcard = "Would you like to attach a postcard?\n" "1) Yes\n" "2) No"

text_prompt_awaiting_postcard = "Please send a postcard picture"

text_prompt_invalid_postcard = (
    "Cannot retrieve postcard image. " "Please send a message with image attached to use it as a postcard"
)


def general_inbound_transactions_format_update(transaction_info: BankTransactionInfo) -> str:
    target_user_requisites = (
        transaction_info.destination.bank_card
        if transaction_info.destination.payment_type == PaymentType.BankCard
        else transaction_info.destination.bank_account
    )
    statement = BankAccountStatementInfo.from_orm(transaction_info, target_user_requisites)
    content = general_account_statement_info_format(statement, target_user_requisites, display_signed_url=False)

    to_str = (
        f"card {transaction_info.destination.bank_card.number_displayable}"
        if transaction_info.destination.payment_type == PaymentType.BankCard
        else f"account {transaction_info.destination.bank_account.id}"
    )

    return f"To a bank {to_str} at {content}"
