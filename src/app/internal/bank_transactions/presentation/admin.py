from django.contrib import admin
from django.contrib.admin import ModelAdmin

from app.internal.bank_transactions.db.models import BankTransaction


@admin.register(BankTransaction)
class BankTransactionAdmin(ModelAdmin):
    list_display = (
        "timestamp",
        "amount",
        "is_committed",
        "source_user",
        "source_account",
        "destination_account",
        "destination_user",
    )
