from typing import Tuple

from prometheus_client.metrics import Gauge, MetricWrapperBase

from app.internal.bank_transactions.domain.services import BankTransactionService
from app.internal.common.presentation.monitoring import IMonitoringFactory


class BankTransactionMonitoringFactory(IMonitoringFactory):
    def __init__(self, transaction_service: BankTransactionService):
        self._transaction_service = transaction_service

    def _get_transactions_sum_10min(self) -> float:
        sum_10min = self._transaction_service.get_transaction_sum_for_last_10_minutes()
        return float(sum_10min)

    def build_metrics(self) -> Tuple[MetricWrapperBase]:
        transaction_sum_10min = Gauge("bank_transactions_sum_10min", "Amount of money transferred for last 10 minutes")
        transaction_sum_10min.set_function(self._get_transactions_sum_10min)

        return (transaction_sum_10min,)
