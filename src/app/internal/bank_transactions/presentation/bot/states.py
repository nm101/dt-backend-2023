import traceback
from decimal import Decimal, InvalidOperation
from typing import Awaitable, Callable, Optional, Tuple, Union

from telegram import Update
from telegram.ext import ContextTypes

import app.internal
import app.internal.bank_accounts.presentation.text_lines_constants
import app.internal.bank_cards.presentation.text_lines_constants
import app.internal.bank_transactions.presentation.text_lines_constants
import app.internal.favourite_users_lists.presentation.text_lines_constants
import app.internal.users.presentation.text_lines_constants
from app.internal.bank_accounts.domain.entities import BankAccountInfo
from app.internal.bank_accounts.domain.services import BankAccountService
from app.internal.bank_cards.domain.entities import BankCardInfo
from app.internal.bank_cards.domain.services import BankCardService
from app.internal.bank_transactions.db.models import BankTransaction
from app.internal.bank_transactions.domain.entities import BankAccountStatementInfo
from app.internal.bank_transactions.domain.payment_type import PaymentType
from app.internal.bank_transactions.domain.services import BankStatisticsService, BankTransactionService
from app.internal.common.domain.bot_persistence_service import TelegramBotPersistenceService
from app.internal.common.domain.conversation_state import ConversationState
from app.internal.common.presentation import text_lines_constants
from app.internal.favourite_users_lists.domain.services import FavouriteUsersListsService
from app.internal.users.domain.entities import TelegramBotUserInfo
from app.internal.users.domain.services import UserService
from app.internal.users.presentation.bot.common import parse_choice_digit


class BotHandlers:
    def __init__(
        self,
        transaction_service: BankTransactionService,
        statistics_service: BankStatisticsService,
        persistence_service: TelegramBotPersistenceService,
        user_service: UserService,
        favourites_service: FavouriteUsersListsService,
        accounts_service: BankAccountService,
        cards_service: BankCardService,
    ):
        self._transaction_service = transaction_service
        self._statistics_service = statistics_service
        self._persistence_service = persistence_service
        self._user_service = user_service
        self._favourites_service = favourites_service
        self._accounts_service = accounts_service
        self._cards_service = cards_service

    async def awaiting_source_type(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> Optional[ConversationState]:
        telegram_id = update.message.from_user.id
        response_digit = await parse_choice_digit(update, context, 2)
        if response_digit is None:
            return

        transaction_pending: BankTransaction = await self._persistence_service.aget_transaction_pending(telegram_id)

        answer = None
        state_next = ConversationState.Transfer_AwaitingSourceChoice

        match response_digit:
            case 1:  # card
                cards = await self._cards_service.alist(telegram_id)
                if not cards:
                    answer = app.internal.bank_cards.presentation.text_lines_constants.general_user_have_no_bank_cards
                    state_next = None
                else:
                    transaction_pending.source_type = PaymentType.BankCard

                    answer = (
                        app.internal.bank_cards.presentation.text_lines_constants.general_cards_list(
                            cards,
                            title=app.internal.bank_transactions.presentation.text_lines_constants.state_transfer_source_type_card_list_title,
                            numbered=True,
                        )
                        + "\n"
                        + text_lines_constants.general_reply_only_with_digit_choice
                    )
            case 2:
                accounts = await self._accounts_service.alist(telegram_id)
                if not accounts:
                    answer = (
                        app.internal.bank_accounts.presentation.text_lines_constants.general_user_have_no_bank_accounts
                    )
                    state_next = None
                else:
                    transaction_pending.source_type = PaymentType.BankAccount

                    answer = (
                        app.internal.bank_accounts.presentation.text_lines_constants.general_accounts_list(
                            accounts,
                            title=app.internal.bank_transactions.presentation.text_lines_constants.state_transfer_source_type_account_list_title,
                            numbered=True,
                        )
                        + "\n"
                        + text_lines_constants.general_reply_only_with_digit_choice
                    )

        await context.bot.send_message(update.message.chat_id, answer)
        return state_next

    async def awaiting_source_type_choice(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> Optional[ConversationState]:
        telegram_id = update.message.from_user.id

        choice = await parse_choice_digit(update, context, 2)
        if choice is None:
            return

        match choice:
            case 1:  # bank card
                next_state = ConversationState.AccountStatement_Card_AwaitingChoice
                service_provider = self._cards_service
                response_formatter = app.internal.bank_cards.presentation.text_lines_constants.general_cards_list
                title = (
                    app.internal.bank_transactions.presentation.text_lines_constants.state_account_statement_awaiting_source_type_card_choice_header
                )

            case 2:  # bank account
                next_state = ConversationState.AccountStatement_Account_AwaitingChoice
                service_provider = self._accounts_service
                response_formatter = app.internal.bank_accounts.presentation.text_lines_constants.general_accounts_list
                title = (
                    app.internal.bank_transactions.presentation.text_lines_constants.state_account_statement_awaiting_source_type_account_choice_header
                )
            case _:
                raise ValueError("Unknown input choice")

        items = await service_provider.alist(telegram_id)

        response = response_formatter(items, title, numbered=True, displaying_to_owner=True)
        await context.bot.send_message(telegram_id, response)

        return next_state

    async def _awaiting_choice_pattern(
        self,
        update: Update,
        context: ContextTypes.DEFAULT_TYPE,
        requisites_service: Union[BankCardService, BankAccountService],
        account_statement_maker: Callable[
            [Union[BankCardInfo, BankAccountInfo]], Awaitable[Tuple[BankAccountStatementInfo, ...]]
        ],
    ) -> Optional[ConversationState]:
        telegram_id = update.message.from_user.id
        items = await requisites_service.alist(telegram_id)

        choice = await parse_choice_digit(update, context, len(items))
        if choice is None:
            return
        item = items[choice - 1]

        account_statement = await account_statement_maker(item)
        response = app.internal.bank_transactions.presentation.text_lines_constants.general_account_statements_format(
            account_statement, item
        )

        await context.bot.send_message(update.message.from_user.id, response)
        return ConversationState.Start

    async def card_awaiting_choice(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> Optional[ConversationState]:
        return await self._awaiting_choice_pattern(
            update, context, self._cards_service, self._statistics_service.aget_account_statement_for_bank_card
        )

    async def account_awaiting_choice(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> Optional[ConversationState]:
        return await self._awaiting_choice_pattern(
            update, context, self._accounts_service, self._statistics_service.aget_account_statement_for_bank_account
        )

    async def to_choose_source_type(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> ConversationState:
        await context.bot.send_message(
            update.message.chat_id,
            app.internal.bank_transactions.presentation.text_lines_constants.state_transfer_general_choose_source_type,
        )

        return ConversationState.Transfer_AwaitingSourceType

    async def to_choose_user_destination_type(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> ConversationState:
        await context.bot.send_message(
            update.message.chat_id,
            app.internal.bank_transactions.presentation.text_lines_constants.general_user_destination_type_text,
        )
        return ConversationState.Transfer_ToParticularTelegramUser_AwaitingDestinationType

    async def awaiting_amount(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> Optional[ConversationState]:
        telegram_id = update.message.from_user.id
        response = update.message.text.strip().replace(" ", "").replace(",", ".")

        transaction_pending = await self._persistence_service.aget_transaction_pending(telegram_id)

        amount = None
        try:
            amount = Decimal(response)
        except InvalidOperation:
            pass
        try:
            await self._transaction_service.aset_amount(transaction_pending, amount)

            await context.bot.send_message(
                update.message.chat_id,
                app.internal.bank_transactions.presentation.text_lines_constants.text_prompt_request_attach_postcard,
            )
            return ConversationState.Transfer_AwaitingChoiceAttachPostcard
        except ValueError:
            await context.bot.send_message(
                update.message.chat_id,
                app.internal.bank_transactions.presentation.text_lines_constants.state_transfer_awaiting_amount_incorrect_message,
            )
            return

    async def awaiting_destination_type(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> Optional[ConversationState]:
        telegram_id = update.message.from_user.id
        response_digit = await app.internal.users.presentation.bot.common.parse_choice_digit(update, context, 4)
        if response_digit is None:
            return

        await self._persistence_service.atransaction_pending_reset(telegram_id)
        await self._persistence_service.atransaction_pending_create_blank(telegram_id)

        match response_digit:
            case 1:
                favourite_list = await self._favourites_service.alist(telegram_id)

                response = app.internal.favourite_users_lists.presentation.text_lines_constants.general_fave_list_list_format(
                    favourite_list,
                    app.internal.bank_transactions.presentation.text_lines_constants.awaiting_destination_type_to_fave_user_title,
                    is_numbered=True,
                )
                state_next = ConversationState.Transfer_ToFavouriteTelegramUser_AwaitingChoice
            case 2:
                response = app.internal.users.presentation.text_lines_constants.general_username_specify_request_text
                state_next = ConversationState.Transfer_ToParticularTelegramUser_AwaitingUsername
            case 3:
                response = (
                    app.internal.bank_transactions.presentation.text_lines_constants.state_awaiting_destination_type_to_bank_card_message
                )
                state_next = ConversationState.Transfer_ToBankCard_AwaitingNumber
            case 4:
                response = (
                    app.internal.bank_transactions.presentation.text_lines_constants.state_awaiting_destination_type_to_bank_account_message
                )
                state_next = ConversationState.Transfer_ToBankAccount_AwaitingAccount
            case _:
                raise ValueError()

        await context.bot.send_message(update.message.chat_id, response)
        return state_next

    async def awaiting_source_choice(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> Optional[ConversationState]:
        response = update.message.text.strip()
        response_digit = None
        try:
            response_digit = int(response)
        except ValueError:
            pass

        telegram_id = update.message.from_user.id

        transaction_pending: BankTransaction = await self._persistence_service.aget_transaction_pending(telegram_id)

        match transaction_pending.source_type:
            case PaymentType.BankCard:
                source = await self._cards_service.alist(telegram_id)
                max_choice = len(source)
                awaitable = self._transaction_service.aset_source_bank_card
            case PaymentType.BankAccount:
                source = await self._accounts_service.alist(telegram_id)
                max_choice = len(source)
                awaitable = self._transaction_service.aset_source_bank_account
            case _:
                raise ValueError("Transaction had unexpected source type")

        if not response_digit or response_digit not in range(1, 1 + max_choice):
            await context.bot.send_message(
                update.message.chat_id,
                text_lines_constants.general_incorrect_response_numeric_range_expected(1, max_choice),
            )
            return

        await awaitable(transaction_pending, source[response_digit - 1])

        await context.bot.send_message(
            update.message.chat_id,
            app.internal.bank_transactions.presentation.text_lines_constants.state_transfer_awaiting_source_choice_amount_question,
        )
        return ConversationState.Transfer_AwaitingAmount

    async def awaiting_confirmation(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> Optional[ConversationState]:
        telegram_id = update.message.from_user.id

        response_number = await app.internal.users.presentation.bot.common.parse_choice_digit(update, context, 2)
        if response_number is None:
            return

        match response_number:
            case 2:  # No
                await context.bot.send_message(
                    update.message.chat_id,
                    app.internal.bank_transactions.presentation.text_lines_constants.state_transfer_awaiting_confirmation_user_decline_message,
                )

                await self._persistence_service.atransaction_pending_reset(telegram_id)
                return ConversationState.Start
            case 1:  # Yes
                transaction_pending = await self._persistence_service.aget_transaction_pending(telegram_id)

                try:
                    await self._transaction_service.acommit(transaction_pending)
                except Exception as e:
                    print(traceback.format_exc())
                    await context.bot.send_message(
                        update.message.chat_id,
                        app.internal.bank_transactions.presentation.text_lines_constants.get_state_transfer_awaiting_confirmation_transaction_fail_message(
                            e
                        ),
                    )
                    return
                await context.bot.send_message(
                    update.message.chat_id,
                    app.internal.bank_transactions.presentation.text_lines_constants.state_transfer_awaiting_confirmation_transaction_success_message,
                )

                await self._persistence_service.atransaction_pending_reset(telegram_id)
                return ConversationState.Start

    async def to_bank_account_awaiting_account(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> Optional[ConversationState]:
        telegram_id = update.message.from_user.id
        account_id = update.message.text.strip()

        account = await self._accounts_service.aget(account_id)
        if not account:
            await context.bot.send_message(
                update.message.chat_id,
                app.internal.bank_transactions.presentation.text_lines_constants.state_transfer_general_bank_account_not_found,
            )
            return

        transaction_pending: BankTransaction = await self._persistence_service.aget_transaction_pending(telegram_id)

        await self._transaction_service.aset_destination_bank_account(transaction_pending, account)

        return await self.to_choose_source_type(update, context)

    async def to_bank_card_awaiting_number(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> Optional[ConversationState]:
        telegram_id = update.message.from_user.id
        response = update.message.text.strip()

        card_number = None
        try:
            card_number = int(response.replace(" ", ""))
        except ValueError:
            pass

        if card_number is None or not self._cards_service.is_valid_number_str(response):
            await context.bot.send_message(
                update.message.chat_id,
                app.internal.bank_transactions.presentation.text_lines_constants.state_to_bank_card_card_number_invalid_message,
            )
            return

        transaction_pending = await self._persistence_service.aget_transaction_pending(telegram_id)

        card = await self._cards_service.aget(card_number)
        if not card:
            await context.bot.send_message(
                update.message.chat_id,
                app.internal.bank_transactions.presentation.text_lines_constants.state_to_bank_card_card_number_does_not_exist_message,
            )
            return
        await self._transaction_service.aset_destination_bank_card(transaction_pending, card)

        return await self.to_choose_source_type(update, context)

    async def to_favourite_user_awaiting_choice(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> Optional[ConversationState]:
        telegram_id = update.message.from_user.id

        favourite_list = await self._favourites_service.alist(telegram_id)
        max_choice = len(favourite_list)

        choice = await app.internal.users.presentation.bot.common.parse_choice_digit(update, context, max_choice)
        if max_choice is None:
            return

        user_chosen: TelegramBotUserInfo = favourite_list[choice - 1]

        transaction_pending: BankTransaction = await self._persistence_service.aget_transaction_pending(telegram_id)

        await self._transaction_service.aset_destination_user(transaction_pending, user_chosen.telegram_id)
        return await self.to_choose_user_destination_type(update, context)

    async def to_particular_user_awaiting_dest_type(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> Optional[ConversationState]:
        telegram_id = update.message.from_user.id

        choice = await app.internal.users.presentation.bot.common.parse_choice_digit(update, context, 2)
        if choice is None:
            return

        transaction_pending: BankTransaction = await self._persistence_service.aget_transaction_pending(telegram_id)
        target_user_id = transaction_pending.destination_user_id

        state_next = None
        answer = None
        match choice:
            case 1:  # bank card
                cards = await self._cards_service.alist(target_user_id)
                answer = app.internal.bank_cards.presentation.text_lines_constants.general_cards_list(
                    cards,
                    app.internal.bank_transactions.presentation.text_lines_constants.transfer_to_user_cards_list_title,
                    displaying_to_owner=False,
                    numbered=True,
                )
                state_next = ConversationState.Transfer_ToParticularTelegramUser_ToCard_AwaitingChoice
            case 2:  # bank account
                accounts = await self._accounts_service.alist(target_user_id)
                answer = app.internal.bank_accounts.presentation.text_lines_constants.general_accounts_list(
                    accounts,
                    app.internal.bank_transactions.presentation.text_lines_constants.transfer_to_user_accounts_list_title,
                    displaying_to_owner=False,
                    numbered=True,
                )
                state_next = ConversationState.Transfer_ToParticularTelegramUser_ToAccount_AwaitingChoice

        await context.bot.send_message(update.message.chat_id, answer)
        return state_next

    async def to_particular_user_awaiting_username(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> Optional[ConversationState]:
        telegram_id = update.message.from_user.id
        user = await app.internal.users.presentation.bot.common.parse_username_retrieve_user(
            update, context, self._user_service
        )
        if not user:
            return

        transaction_pending = await self._persistence_service.aget_transaction_pending(telegram_id)

        await self._transaction_service.aset_destination_user(transaction_pending, user.telegram_id)

        return await self.to_choose_user_destination_type(update, context)

    async def to_particular_user_to_account_awaiting_choice(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> Optional[ConversationState]:
        telegram_id = update.message.from_user.id
        transaction_pending: BankTransaction = await self._persistence_service.aget_transaction_pending(telegram_id)
        target_user_id = transaction_pending.destination_user_id

        accounts = await self._accounts_service.alist(target_user_id)
        max_choice = len(accounts)

        choice = await app.internal.users.presentation.bot.common.parse_choice_digit(update, context, max_choice)
        if choice is None:
            return

        account_chosen: BankAccountInfo = accounts[choice - 1]
        await self._transaction_service.aset_destination_bank_account(transaction_pending, account_chosen)

        return await self.to_choose_source_type(update, context)

    async def to_particular_user_to_card_awaiting_choice(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> Optional[ConversationState]:
        telegram_id = update.message.from_user.id
        transaction_pending: BankTransaction = await self._persistence_service.aget_transaction_pending(telegram_id)
        target_user_id = transaction_pending.destination_user_id

        cards = await self._cards_service.alist(target_user_id)
        max_choice = len(cards)

        choice = await app.internal.users.presentation.bot.common.parse_choice_digit(update, context, max_choice)
        if choice is None:
            return

        card_chosen: BankCardInfo = cards[choice - 1]
        await self._transaction_service.aset_destination_bank_card(transaction_pending, card_chosen)

        return await self.to_choose_source_type(update, context)

    async def awaiting_choice_attach_postcard(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> Optional[ConversationState]:

        response_number = await app.internal.users.presentation.bot.common.parse_choice_digit(update, context, 2)
        if response_number is None:
            return

        match response_number:
            case 2:  # No
                await context.bot.send_message(
                    update.message.chat_id,
                    app.internal.bank_transactions.presentation.text_lines_constants.state_transaction_confirmation_request,
                )

                return ConversationState.Transfer_AwaitingTransactionConfirmation
            case 1:  # Yes

                await context.bot.send_message(
                    update.message.chat_id,
                    app.internal.bank_transactions.presentation.text_lines_constants.text_prompt_awaiting_postcard,
                )
                return ConversationState.Transfer_AwaitingPostcard

    async def awaiting_postcard(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> Optional[ConversationState]:
        telegram_id = update.message.from_user.id
        transaction_pending = await self._persistence_service.aget_transaction_pending(telegram_id)

        if not update.message.photo:
            await context.bot.send_message(
                update.message.from_user.id,
                app.internal.bank_transactions.presentation.text_lines_constants.text_prompt_invalid_postcard,
            )
            return

        photo_obj = update.message.photo[-1]
        photo_file = await photo_obj.get_file()
        photo_bytes = await photo_file.download_as_bytearray()
        self._transaction_service.set_postcard(transaction_pending, str(photo_obj.file_unique_id), bytes(photo_bytes))

        await context.bot.send_message(
            update.message.chat_id,
            app.internal.bank_transactions.presentation.text_lines_constants.state_transaction_confirmation_request,
        )
        return ConversationState.Transfer_AwaitingTransactionConfirmation
