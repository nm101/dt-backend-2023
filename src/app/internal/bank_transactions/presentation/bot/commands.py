from io import BytesIO
from typing import Optional

from telegram import Update
from telegram.ext import ContextTypes

import app.internal.bank_transactions.presentation.text_lines_constants
import app.internal.users.presentation.text_lines_constants
from app.internal.bank_transactions.domain.entities import BankTransactionInfo
from app.internal.bank_transactions.domain.services import BankStatisticsService, BankTransactionService
from app.internal.common.domain.conversation_state import ConversationState
from app.internal.users.domain.services import UserService


class BotHandlers:
    def __init__(
        self,
        user_service: UserService,
        transaction_service: BankTransactionService,
        statistics_service: BankStatisticsService,
    ):
        self._user_service = user_service
        self._transaction_service = transaction_service
        self._statistics_service = statistics_service

    async def transfer(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> Optional[ConversationState]:
        """Initiate money transfer"""

        telegram_id = update.message.from_user.id

        if not await self._user_service.ais_phone_number_set(telegram_id):
            await context.bot.send_message(
                update.message.chat_id,
                app.internal.users.presentation.text_lines_constants.general_command_phone_number_not_set_error_message,
            )
            return

        await context.bot.send_message(
            update.message.chat_id,
            app.internal.bank_transactions.presentation.text_lines_constants.command_transfer_specify_destination_type,
        )
        return ConversationState.Transfer_AwaitingDestinationType

    async def related_users(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        telegram_id = update.message.from_user.id

        users = await self._statistics_service.aget_related_users_info(telegram_id)
        response = app.internal.bank_transactions.presentation.text_lines_constants.command_related_users_list_form(
            users
        )

        await context.bot.send_message(telegram_id, response)

    async def account_statement(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> ConversationState:
        telegram_id = update.message.from_user.id
        await context.bot.send_message(
            telegram_id,
            app.internal.bank_transactions.presentation.text_lines_constants.command_account_statement_text,
        )
        return ConversationState.AccountStatement_AwaitingSourceTypeChoice

    async def transfers_new(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        telegram_id = update.message.from_user.id

        transactions = await self._transaction_service.anext_not_seen_inbound_transactions_for_user(telegram_id)
        if not transactions:
            await context.bot.send_message(update.message.chat_id, "No new transactions found")
            return

        for item in transactions:
            item: BankTransactionInfo
            text = app.internal.bank_transactions.presentation.text_lines_constants.general_inbound_transactions_format_update(
                item,
            )
            if item.postcard is not None:
                await context.bot.send_photo(
                    chat_id=update.message.chat_id, photo=BytesIO(item.postcard.content), caption=text
                )
            else:
                await context.bot.send_message(update.message.chat_id, text)
