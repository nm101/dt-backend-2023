from datetime import timedelta

from django.db import models
from django.db.models import DateTimeField, Q
from django.db.models.functions import Cast
from django.utils import timezone

from app.internal.bank_accounts.db.models import BankAccount
from app.internal.bank_cards.db.models import BankCard
from app.internal.bank_transactions.domain.payment_type import PaymentType
from app.internal.users.db.models import TelegramBotUser

ACCOUNT_STATEMENT_PERIOD_DAYS = 30


class ByAccountStatementPeriodManager(models.Manager):
    def get_queryset(self):
        least_timestamp = timezone.now() - timedelta(days=ACCOUNT_STATEMENT_PERIOD_DAYS)
        return (
            super()
            .get_queryset()
            .filter(Q(timestamp__gte=Cast(least_timestamp, DateTimeField())) & Q(is_committed=True))
        )


class BankTransaction(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True, editable=False, db_index=True)
    is_committed = models.BooleanField(default=False)
    amount = models.DecimalField(max_digits=66, decimal_places=2, default=0)

    source_user = models.ForeignKey(
        to=TelegramBotUser, on_delete=models.CASCADE, related_name="source_user", db_index=True
    )
    destination_user = models.ForeignKey(
        to=TelegramBotUser,
        on_delete=models.CASCADE,
        related_name="destination_user",
        db_index=True,
    )

    source_type = models.SmallIntegerField(choices=tuple((e.value, e.name) for e in PaymentType), null=True, blank=True)
    destination_type = models.SmallIntegerField(
        choices=tuple((e.value, e.name) for e in PaymentType), null=True, blank=True
    )
    source_account = models.ForeignKey(
        to=BankAccount, on_delete=models.CASCADE, null=True, blank=True, related_name="source_account", db_index=True
    )
    destination_account = models.ForeignKey(
        to=BankAccount,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name="destination_account",
        db_index=True,
    )

    source_card = models.ForeignKey(
        to=BankCard, on_delete=models.CASCADE, null=True, blank=True, related_name="source_card", db_index=True
    )
    destination_card = models.ForeignKey(
        to=BankCard, on_delete=models.CASCADE, null=True, blank=True, related_name="destination_card", db_index=True
    )

    postcard = models.ImageField(upload_to="postcards", blank=True)
    is_seen = models.BooleanField(default=False)

    objects = models.Manager()
    by_account_statement_period_objects = ByAccountStatementPeriodManager()

    class Meta:
        unique_together = ("timestamp", "source_user")
