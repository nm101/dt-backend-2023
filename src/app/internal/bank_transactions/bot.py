from typing import Dict, List, Tuple

from telegram.ext import CommandHandler

from app.internal.common.domain.conversation_state import ConversationState as State

from ..bank_accounts.domain.services import BankAccountService
from ..bank_cards.domain.services import BankCardService
from ..common.domain.bot_persistence_service import TelegramBotPersistenceService
from ..favourite_users_lists.domain.services import FavouriteUsersListsService
from ..users.domain.services import UserService
from .domain.services import BankStatisticsService, BankTransactionService
from .presentation.bot import commands, states


def build_handlers(
    transaction_service: BankTransactionService,
    statistics_service: BankStatisticsService,
    persistence_service: TelegramBotPersistenceService,
    user_service: UserService,
    favourites_service: FavouriteUsersListsService,
    accounts_service: BankAccountService,
    cards_service: BankCardService,
) -> Tuple[List, Dict]:
    c = commands.BotHandlers(user_service, transaction_service, statistics_service)
    s = states.BotHandlers(
        transaction_service,
        statistics_service,
        persistence_service,
        user_service,
        favourites_service,
        accounts_service,
        cards_service,
    )

    command_handlers = [
        CommandHandler("transfer", c.transfer),
        CommandHandler("related_users", c.related_users),
        CommandHandler("account_statement", c.account_statement),
        CommandHandler("transfers_new", c.transfers_new),
    ]

    state_handlers = {
        # /transfer command conversation states
        State.Transfer_AwaitingDestinationType: s.awaiting_destination_type,
        State.Transfer_AwaitingSourceType: s.awaiting_source_type,
        State.Transfer_AwaitingSourceChoice: s.awaiting_source_choice,
        State.Transfer_AwaitingAmount: s.awaiting_amount,
        State.Transfer_AwaitingTransactionConfirmation: s.awaiting_confirmation,
        State.Transfer_ToBankAccount_AwaitingAccount: s.to_bank_account_awaiting_account,
        State.Transfer_ToBankCard_AwaitingNumber: s.to_bank_card_awaiting_number,
        State.Transfer_ToParticularTelegramUser_AwaitingUsername: s.to_particular_user_awaiting_username,
        State.Transfer_ToParticularTelegramUser_AwaitingDestinationType: s.to_particular_user_awaiting_dest_type,
        State.Transfer_ToParticularTelegramUser_ToAccount_AwaitingChoice: s.to_particular_user_to_account_awaiting_choice,
        State.Transfer_ToParticularTelegramUser_ToCard_AwaitingChoice: s.to_particular_user_to_card_awaiting_choice,
        State.Transfer_ToFavouriteTelegramUser_AwaitingChoice: s.to_favourite_user_awaiting_choice,
        State.Transfer_AwaitingChoiceAttachPostcard: s.awaiting_choice_attach_postcard,
        State.Transfer_AwaitingPostcard: s.awaiting_postcard,
        # /account_statement conversation states
        State.AccountStatement_AwaitingSourceTypeChoice: s.awaiting_source_type_choice,
        State.AccountStatement_Card_AwaitingChoice: s.card_awaiting_choice,
        State.AccountStatement_Account_AwaitingChoice: s.account_awaiting_choice,
    }

    return command_handlers, state_handlers
