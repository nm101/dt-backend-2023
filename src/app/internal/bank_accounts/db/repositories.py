from decimal import Decimal
from typing import Optional, Tuple
from uuid import UUID

from django.core.exceptions import ValidationError
from django.db.models import Sum

from app.internal.bank_accounts.db.models import BankAccount, BankCurrency, BankExchangeRate
from app.internal.bank_accounts.domain.entities import BankAccountInfo
from app.internal.bank_accounts.domain.services import IBankAccountRepository, IBankExchangeRateRepository


class BankExchangeRateRepository(IBankExchangeRateRepository):
    def get(self, source: BankCurrency, destination: BankCurrency) -> Optional[BankExchangeRate]:
        return BankExchangeRate.objects.filter(source=source, destination=destination).first()


class BankAccountRepository(IBankAccountRepository):
    def get_total_money_sum(self) -> Decimal:
        aggregation = BankAccount.objects.aggregate(total_money=Sum("balance"))
        result = aggregation["total_money"]
        if result is None:
            return Decimal("0")
        return result

    def get(self, account_id: UUID) -> Optional[BankAccountInfo]:
        """
        Retrieve bank account info by account ID
        Returns None if account not exists
        """
        try:
            obj = BankAccount.objects.filter(id=account_id).select_related("owner", "currency").first()
            return BankAccountInfo.from_orm(obj)
        except ValidationError:
            return

    def list(self, telegram_id: int) -> Tuple[BankAccountInfo, ...]:
        queryset = BankAccount.objects.filter(owner_id=telegram_id).all()
        return tuple(BankAccountInfo.from_orm(entity) for entity in queryset)
