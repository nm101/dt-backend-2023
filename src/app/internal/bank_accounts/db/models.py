import uuid
from decimal import Decimal

from django.core.validators import MinValueValidator
from django.db import models

from app.internal.users.db.models import TelegramBotUser


class BankCurrency(models.Model):
    name = models.CharField(max_length=64)
    iso_code = models.CharField(max_length=3, blank=True)

    def __str__(self):
        return f"{self.name} ({self.iso_code})" if self.iso_code else self.name

    class Meta:
        verbose_name = "Bank currency"
        verbose_name_plural = "Bank currencies"


class BankExchangeRate(models.Model):
    source = models.ForeignKey(to=BankCurrency, on_delete=models.PROTECT, related_name="source")
    destination = models.ForeignKey(to=BankCurrency, on_delete=models.PROTECT, related_name="destination")
    rate = models.DecimalField(
        max_digits=66, decimal_places=2, default=1, validators=[MinValueValidator(Decimal("0.01"))]
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["source", "destination"], name="unique_source_destination_combination")
        ]

    def __str__(self):
        return f"{self.source.name} -[{self.rate}]-> {self.destination.name}"


class BankAccount(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    owner = models.ForeignKey(to=TelegramBotUser, on_delete=models.PROTECT)

    currency = models.ForeignKey(to=BankCurrency, on_delete=models.PROTECT)
    balance = models.DecimalField(max_digits=66, decimal_places=2, default=0)

    def __str__(self):
        return f"{self.id}"
