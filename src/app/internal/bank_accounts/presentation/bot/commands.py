from asgiref.sync import sync_to_async
from telegram import Update
from telegram.ext import ContextTypes

import app.internal.bank_accounts.presentation.text_lines_constants
import app.internal.users.presentation.text_lines_constants
from app.internal.bank_accounts.domain.services import BankAccountService
from app.internal.users.domain.exceptions import PhoneNumberNotSet


class BotHandlers:
    def __init__(self, accounts_service: BankAccountService):
        self._accounts_service = accounts_service

    async def accounts_list(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        """
        Displays all bank accounts owned by user
        """
        telegram_id = update.message.from_user.id

        try:
            accounts = await self._accounts_service.alist(telegram_id)
        except PhoneNumberNotSet:
            response_message = (
                app.internal.users.presentation.text_lines_constants.general_command_phone_number_not_set_error_message
            )
        else:
            if accounts:
                response_message = app.internal.bank_accounts.presentation.text_lines_constants.general_accounts_list(
                    accounts, app.internal.bank_accounts.presentation.text_lines_constants.command_accounts_list_title
                )
            else:
                response_message = (
                    app.internal.bank_accounts.presentation.text_lines_constants.general_user_have_no_bank_accounts
                )

        await context.bot.send_message(update.message.chat_id, response_message)
