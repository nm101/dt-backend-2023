from django.contrib import admin
from django.contrib.admin import ModelAdmin

from app.internal.bank_accounts.db.models import BankAccount, BankCurrency, BankExchangeRate


@admin.register(BankCurrency)
class BankCurrencyAdmin(ModelAdmin):
    list_display = ("name", "iso_code")


@admin.register(BankExchangeRate)
class BankExchangeRateAdmin(ModelAdmin):
    list_display = ("source", "destination", "rate")
    pass


@admin.register(BankAccount)
class BankAccountAdmin(ModelAdmin):
    list_display = ("__str__", "owner", "currency", "balance")
