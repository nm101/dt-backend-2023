from typing import Tuple

from prometheus_client.metrics import Gauge, MetricWrapperBase

from app.internal.bank_accounts.domain.services import BankAccountService
from app.internal.common.presentation.monitoring import IMonitoringFactory


class BankAccountsMonitoringFactory(IMonitoringFactory):
    def __init__(self, account_service: BankAccountService):
        self._account_service = account_service

    def _get_total_money(self) -> float:
        total_money_sum = self._account_service.get_total_money_sum()
        return float(total_money_sum)

    def build_metrics(self) -> Tuple[MetricWrapperBase]:
        bank_total_money = Gauge("bank_total_money", "Total amount of money in the bank")
        bank_total_money.set_function(self._get_total_money)

        return (bank_total_money,)
