from typing import List

from ninja_extra import NinjaExtraAPI, Router
from ninja_jwt.authentication import JWTAuth

from app.internal.bank_accounts.domain.entities import BankAccountInfo
from app.internal.bank_accounts.presentation.handlers import BankAccountHandlers
from app.internal.common.domain.entities import ErrorResponse


def get_accounts_router(accounts_handlers: BankAccountHandlers) -> Router:
    router = Router(tags=["accounts"])

    router.add_api_operation(
        "",
        ["GET"],
        accounts_handlers.list,
        url_name="accounts_list",
        response={200: List[BankAccountInfo], 403: ErrorResponse},
        auth=JWTAuth(),
    )

    return router


def add_accounts_router(api: NinjaExtraAPI, accounts_handlers: BankAccountHandlers):
    accounts_handler = get_accounts_router(accounts_handlers)
    api.add_router("/accounts", accounts_handler, tags=accounts_handler.tags)
