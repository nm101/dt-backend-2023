from typing import List

from django.http import HttpRequest

from app.internal.bank_accounts.domain.entities import BankAccountInfo
from app.internal.bank_accounts.domain.services import BankAccountService


class BankAccountHandlers:
    def __init__(self, account_service: BankAccountService):
        self._account_service = account_service

    def list(self, request: HttpRequest) -> List[BankAccountInfo]:
        telegram_id = request.user.telegram_id

        return list(self._account_service.list(telegram_id))
