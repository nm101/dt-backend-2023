from typing import Tuple

from app.internal.bank_accounts.domain.entities import BankAccountInfo


def bot_accounts_list_response_template(
    account: BankAccountInfo, displaying_to_owner: bool = False, numbering_number: int = None
):
    numbering_str = f"{numbering_number}) " if numbering_number is not None else ""
    return (
        (numbering_str + f"ID {account.id}:\n" f"  Balance: {account.balance_displayable}\n")
        if displaying_to_owner
        else (numbering_str + f"ID {account.id}\n" f"  Currency: {account.currency_str}")
    )


command_accounts_list_title = "Accounts available:"


def general_accounts_list(
    accounts: Tuple[BankAccountInfo, ...], title: str, numbered: bool = False, displaying_to_owner: bool = True
) -> str:
    count = [0]

    def map_func(account):
        count[0] += 1
        numbering_number = count[0] if numbered else None
        return bot_accounts_list_response_template(account, displaying_to_owner, numbering_number)

    return "\n".join(
        (
            f"{title}\n",
            "\n".join(map(map_func, accounts)),
        )
    )


general_user_have_no_bank_accounts = "You don't have any bank accounts yet"
