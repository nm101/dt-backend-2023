from __future__ import annotations

from ninja.orm import create_schema

from app.internal.bank_accounts.db.models import BankAccount, BankCurrency, BankExchangeRate
from app.internal.users.domain.entities import TelegramBotUserInfo

BankCurrencyInfo = create_schema(BankCurrency)
BankExchangeRateInfo = create_schema(BankExchangeRate, depth=1)

BankAccountSchema = create_schema(
    BankAccount,
    depth=1,
    fields=["id", "owner", "currency", "balance"],
    custom_fields=[("owner", TelegramBotUserInfo, None), ("currency", BankCurrencyInfo, None)],
)


class BankAccountInfo(BankAccountSchema):
    @property
    def owner_id(self):
        return self.owner.telegram_id

    @property
    def currency_str(self):
        return display_in_balance(self.currency)

    @property
    def balance_displayable(self):
        return balance_displayable(self)


def display_in_balance(currency: BankCurrencyInfo):
    return currency.iso_code if currency.iso_code else currency.name


def balance_displayable(account: BankAccountInfo):
    currency_str = display_in_balance(account.currency)
    return f"{account.balance} {currency_str}"
