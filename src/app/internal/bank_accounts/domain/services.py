from __future__ import annotations

from abc import ABC
from decimal import Decimal
from typing import Optional, Tuple
from uuid import UUID

from asgiref.sync import sync_to_async

from app.internal.bank_accounts.db.models import BankCurrency, BankExchangeRate
from app.internal.bank_accounts.domain.entities import BankAccountInfo
from app.internal.bank_accounts.domain.exceptions import CurrencyConversionImpossible
from app.internal.users.domain.exceptions import PhoneNumberNotSet
from app.internal.users.domain.services import UserService


class IBankExchangeRateRepository(ABC):
    def get(self, source: BankCurrency, destination: BankCurrency) -> Optional[BankExchangeRate]:
        ...


class BankExchangeRateService:
    def __init__(self, exchange_rate_repo: IBankExchangeRateRepository):
        self._exchange_rate_repo = exchange_rate_repo

    def get(self, source: BankCurrency, destination: BankCurrency) -> Decimal:
        obj = self._exchange_rate_repo.get(source, destination)
        if obj is None:
            if source == destination:
                return Decimal(1)
            raise CurrencyConversionImpossible(f"{source} -X-> {destination}")
        return obj.rate

    @sync_to_async
    def aget(self, source: BankCurrency, destination: BankCurrency) -> Decimal:
        return self.get(source, destination)


class IBankAccountRepository(ABC):
    def get(self, account_id: UUID) -> Optional[BankAccountInfo]:
        """
        Retrieve bank account info by account ID
        Returns None if account not exists
        """
        ...

    def list(self, telegram_id: int) -> Tuple[BankAccountInfo, ...]:
        ...

    def get_total_money_sum(self) -> Decimal:
        ...


class BankAccountService:
    """
    Bank accounts service
    """

    def __init__(self, account_repo: IBankAccountRepository, user_service: UserService):
        self._account_repo = account_repo
        self._user_service = user_service

    def get_total_money_sum(self) -> Decimal:
        return self._account_repo.get_total_money_sum()

    def get(self, account_id: UUID) -> Optional[BankAccountInfo]:
        """
        Retrieve bank account info by account ID
        Returns None if account not exists
        """
        return self._account_repo.get(account_id)

    @sync_to_async
    def aget(self, account_id: UUID) -> BankAccountInfo:
        return self.get(account_id)

    def list(self, telegram_id: int) -> Tuple[BankAccountInfo, ...]:
        if not self._user_service.is_phone_number_set(telegram_id):
            raise PhoneNumberNotSet()

        return self._account_repo.list(telegram_id)

    @sync_to_async
    def alist(self, telegram_id: int) -> Tuple[BankAccountInfo, ...]:
        return self.list(telegram_id)
