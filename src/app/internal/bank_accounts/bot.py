from typing import Dict, List, Tuple

from telegram.ext import CommandHandler

from .domain.services import BankAccountService
from .presentation.bot import commands


def build_handlers(accounts_service: BankAccountService) -> Tuple[List, Dict]:
    command_handlers_class = commands.BotHandlers(accounts_service)

    command_handlers = [
        CommandHandler("accounts_list", command_handlers_class.accounts_list),
    ]

    state_handlers = {}

    return command_handlers, state_handlers
