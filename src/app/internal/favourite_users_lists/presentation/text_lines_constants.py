from typing import Tuple

from app.internal.users.domain.entities import TelegramBotUserInfo
from app.internal.users.presentation.text_lines_constants import general_user_displayable


def general_fave_list_list_format(
    favourites_list: Tuple[TelegramBotUserInfo, ...], title_text: str, is_numbered: bool = False
) -> str:
    count = [0]

    def wrap_count(user_info: TelegramBotUserInfo):
        count[0] += 1
        return f"{count[0]}) {general_user_displayable(user_info)}"

    map_func = wrap_count if is_numbered else general_user_displayable
    return f"{title_text}\n" + "\n".join(map_func(user_info) for user_info in favourites_list)


general_fave_list_empty_message = "Favourites list is empty"
command_fave_list_title = "Your favourite users are:"
command_fave_delete_title = "Please specify user's number in list below to delete:"
state_favourite_list_add_awaiting_username_success_text = "Successfully added to favourites list"
state_favourite_list_delete_success_text = "Deleted successfully"
