from django.contrib import admin
from django.contrib.admin import ModelAdmin

from app.internal.favourite_users_lists.db.models import FavouriteAccountsList


@admin.register(FavouriteAccountsList)
class FavouriteAccountsListAdmin(ModelAdmin):
    pass
