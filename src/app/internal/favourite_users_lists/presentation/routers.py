from typing import List

from ninja_extra import NinjaExtraAPI, Router
from ninja_jwt.authentication import JWTAuth

from app.internal.common.domain.entities import ErrorResponse
from app.internal.common.domain.exceptions import NotFoundException
from app.internal.favourite_users_lists.presentation.handlers import FavouritesHandlers
from app.internal.users.domain.entities import TelegramBotUserInfo, TelegramBotUserSelectorUsername


def get_favourites_router(favourites_handlers: FavouritesHandlers) -> Router:
    router = Router(tags=["favourites"])

    router.add_api_operation(
        "",
        ["GET"],
        favourites_handlers.list,
        url_name="fave_list",
        response={200: List[TelegramBotUserInfo], 403: ErrorResponse},
        auth=JWTAuth(),
    )

    router.add_api_operation(
        "",
        ["POST"],
        favourites_handlers.add,
        url_name="fave_add",
        response={200: TelegramBotUserSelectorUsername, 404: ErrorResponse, 403: ErrorResponse},
        auth=JWTAuth(),
    )

    router.add_api_operation(
        "",
        ["DELETE"],
        favourites_handlers.delete,
        url_name="fave_delete",
        response={200: TelegramBotUserSelectorUsername, 404: ErrorResponse, 403: ErrorResponse},
        auth=JWTAuth(),
    )

    return router


def add_favourites_router(api: NinjaExtraAPI, favourites_handlers: FavouritesHandlers):
    favourites_handler = get_favourites_router(favourites_handlers)
    api.add_router("/favourites", favourites_handler, tags=favourites_handler.tags)
