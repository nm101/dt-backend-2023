from typing import List

from django.http import HttpRequest

from app.internal.favourite_users_lists.domain.services import FavouriteUsersListsService
from app.internal.users.domain.entities import TelegramBotUserInfo, TelegramBotUserSelectorUsername


class FavouritesHandlers:
    def __init__(self, fave_service: FavouriteUsersListsService):
        self._fave_service = fave_service

    def list(self, request: HttpRequest) -> List[TelegramBotUserInfo]:
        telegram_id = request.user.telegram_id

        return list(self._fave_service.list(telegram_id))

    def add(self, request: HttpRequest, selector: TelegramBotUserSelectorUsername) -> TelegramBotUserSelectorUsername:
        telegram_id = request.user.telegram_id

        self._fave_service.add_by_username(telegram_id, selector.username)
        return selector

    def delete(
        self, request: HttpRequest, selector: TelegramBotUserSelectorUsername
    ) -> TelegramBotUserSelectorUsername:
        telegram_id = request.user.telegram_id

        self._fave_service.delete_by_username(telegram_id, selector.username)
        return selector
