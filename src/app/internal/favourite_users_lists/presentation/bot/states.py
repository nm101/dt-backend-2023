from typing import Optional

from telegram import Update
from telegram.ext import ContextTypes

import app.internal
import app.internal.favourite_users_lists.presentation.text_lines_constants
import app.internal.users.presentation.bot.common
from app.internal.common.domain.conversation_state import ConversationState
from app.internal.favourite_users_lists.domain.services import FavouriteUsersListsService
from app.internal.users.domain.services import UserService


class BotHandlers:
    def __init__(
        self,
        user_service: UserService,
        favourites_service: FavouriteUsersListsService,
    ):
        self._favourites_service = favourites_service
        self._user_service = user_service

    async def favourites_list_add_awaiting_username(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> Optional[ConversationState]:
        telegram_id = update.message.from_user.id
        user_to_add = await app.internal.users.presentation.bot.common.parse_username_retrieve_user(
            update, context, self._user_service
        )
        if not user_to_add:
            return

        await self._favourites_service.aadd(telegram_id, user_to_add.telegram_id)

        await context.bot.send_message(
            update.message.chat_id,
            app.internal.favourite_users_lists.presentation.text_lines_constants.state_favourite_list_add_awaiting_username_success_text,
        )
        return ConversationState.Start

    async def favourites_list_delete_awaiting_choice(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> Optional[ConversationState]:
        telegram_id = update.message.from_user.id
        favourites_list = await self._favourites_service.alist(telegram_id)

        max_choice = len(favourites_list)

        choice = await app.internal.users.presentation.bot.common.parse_choice_digit(update, context, max_choice)
        if choice is None:
            return

        await self._favourites_service.adelete_by_number_in_list(telegram_id, choice - 1)
        await context.bot.send_message(
            update.message.chat_id,
            app.internal.favourite_users_lists.presentation.text_lines_constants.state_favourite_list_delete_success_text,
        )
        return ConversationState.Start
