from typing import Optional

from telegram import Update
from telegram.ext import ContextTypes

import app.internal.favourite_users_lists.presentation.text_lines_constants
import app.internal.users.presentation.text_lines_constants
from app.internal.common.domain.conversation_state import ConversationState
from app.internal.favourite_users_lists.domain.services import FavouriteUsersListsService
from app.internal.users.domain.exceptions import PhoneNumberNotSet
from app.internal.users.domain.services import UserService


class BotHandlers:
    def __init__(self, user_service: UserService, favourites_service: FavouriteUsersListsService):
        self._favourites_service = favourites_service
        self._user_service = user_service

    async def fave_list(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        telegram_id = update.message.from_user.id

        try:
            favourites_list = await self._favourites_service.alist(telegram_id)
        except PhoneNumberNotSet:
            await context.bot.send_message(
                update.message.chat_id,
                app.internal.users.presentation.text_lines_constants.general_command_phone_number_not_set_error_message,
            )
            return

        response = app.internal.favourite_users_lists.presentation.text_lines_constants.general_fave_list_empty_message
        if favourites_list:
            response = app.internal.favourite_users_lists.presentation.text_lines_constants.general_fave_list_list_format(
                favourites_list,
                title_text=app.internal.favourite_users_lists.presentation.text_lines_constants.command_fave_list_title,
            )

        await context.bot.send_message(update.message.chat_id, response)

    async def fave_add(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> Optional[ConversationState]:
        telegram_id = update.message.from_user.id

        if not await self._user_service.ais_phone_number_set(telegram_id):
            await context.bot.send_message(
                update.message.chat_id,
                app.internal.users.presentation.text_lines_constants.general_command_phone_number_not_set_error_message,
            )
            return

        await context.bot.send_message(
            update.message.chat_id,
            app.internal.users.presentation.text_lines_constants.general_username_specify_request_text,
        )
        return ConversationState.FavoritesList_Add_AwaitingUsername

    async def fave_del(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> Optional[ConversationState]:
        telegram_id = update.message.from_user.id

        try:
            favourites_list = await self._favourites_service.alist(telegram_id)
        except PhoneNumberNotSet:
            await context.bot.send_message(
                update.message.chat_id,
                app.internal.users.presentation.text_lines_constants.general_command_phone_number_not_set_error_message,
            )
            return

        if not favourites_list:
            await context.bot.send_message(
                update.message.chat_id,
                app.internal.favourite_users_lists.presentation.text_lines_constants.general_fave_list_empty_message,
            )
            return

        await context.bot.send_message(
            update.message.chat_id,
            app.internal.favourite_users_lists.presentation.text_lines_constants.general_fave_list_list_format(
                favourites_list,
                app.internal.favourite_users_lists.presentation.text_lines_constants.command_fave_delete_title,
                is_numbered=True,
            ),
        )
        return ConversationState.FavoritesList_Delete_AwaitingChoice
