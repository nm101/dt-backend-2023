from typing import Optional, Tuple

from django.db import IntegrityError
from django.db.models import QuerySet

from app.internal.favourite_users_lists.db.models import FavouriteAccountsList
from app.internal.favourite_users_lists.domain.services import IFavouriteUsersListsRepository
from app.internal.users.db.models import TelegramBotUser
from app.internal.users.domain.entities import TelegramBotUserInfo


class FavouriteUsersListsRepository(IFavouriteUsersListsRepository):
    def _list(self, telegram_id: int) -> Optional[QuerySet]:
        obj = FavouriteAccountsList.objects.filter(owner_id=telegram_id).first()
        if not obj:
            return

        return obj.favourites_list.all()

    def list(self, telegram_id: int) -> Tuple[TelegramBotUserInfo, ...]:
        items = self._list(telegram_id)
        if items is None:
            return ()
        return tuple(TelegramBotUserInfo.from_orm(entity) for entity in items)

    def add(self, owner_telegram_id: int, add_telegram_id: int) -> None:
        entity, has_created = FavouriteAccountsList.objects.get_or_create(owner_id=owner_telegram_id)
        try:
            entity.favourites_list.add(add_telegram_id)
        except IntegrityError:
            raise ValueError("User with specified ID does not exist")

    def delete(self, telegram_id: int, deletable_telegram_id: int) -> None:
        entity = FavouriteAccountsList.objects.get(owner_id=telegram_id)
        if entity is None:
            return

        entity.favourites_list.remove(deletable_telegram_id)

    def delete_by_number_in_list(self, telegram_id: int, number_in_list: int) -> None:
        """
        Removes entry from favourite list by its position
        If favourites list does not exist, does nothing
        Raise ValueError if number is incorrect
        """

        if number_in_list < 0:
            raise ValueError("number_in_list must be non-negative")

        items = self._list(telegram_id)
        if items is None:
            return

        user_entity: TelegramBotUser = items[number_in_list]
        self.delete(telegram_id, user_entity.telegram_id)
