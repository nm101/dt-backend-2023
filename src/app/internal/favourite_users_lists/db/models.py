from django.db import models

from app.internal.users.db.models import TelegramBotUser


class FavouriteAccountsList(models.Model):
    owner = models.OneToOneField(
        to=TelegramBotUser, on_delete=models.CASCADE, related_name="favourite_owner", primary_key=True
    )
    favourites_list = models.ManyToManyField(to=TelegramBotUser, related_name="favourites_list")

    def __str__(self):
        return f"Favourite accounts list of {self.owner}"
