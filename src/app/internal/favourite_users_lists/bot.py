from typing import Dict, List, Tuple

from telegram.ext import CommandHandler

from app.internal.common.domain.conversation_state import ConversationState as State

from ..users.domain.services import UserService
from .domain.services import FavouriteUsersListsService
from .presentation.bot import commands, states


def build_handlers(favorites_service: FavouriteUsersListsService, user_service: UserService) -> Tuple[List, Dict]:
    command_handlers_class = commands.BotHandlers(user_service, favorites_service)
    state_handlers_class = states.BotHandlers(user_service, favorites_service)

    command_handlers = [
        CommandHandler("fave_list", command_handlers_class.fave_list),
        CommandHandler("fave_add", command_handlers_class.fave_add),
        CommandHandler("fave_del", command_handlers_class.fave_del),
    ]

    state_handlers = {
        # /fave_add, /fave_del conversation states
        State.FavoritesList_Add_AwaitingUsername: state_handlers_class.favourites_list_add_awaiting_username,
        State.FavoritesList_Delete_AwaitingChoice: state_handlers_class.favourites_list_delete_awaiting_choice,
    }

    return command_handlers, state_handlers
