from abc import ABC
from typing import Tuple

from asgiref.sync import sync_to_async

from app.internal.common.domain.exceptions import NotFoundException
from app.internal.users.domain.entities import TelegramBotUserInfo
from app.internal.users.domain.exceptions import PhoneNumberNotSet
from app.internal.users.domain.services import UserService


class IFavouriteUsersListsRepository(ABC):
    def list(self, telegram_id: int) -> Tuple[TelegramBotUserInfo, ...]:
        ...

    def add(self, owner_telegram_id: int, add_telegram_id: int) -> None:
        ...

    def delete(self, telegram_id: int, deletable_telegram_id: int) -> None:
        ...

    def delete_by_number_in_list(self, telegram_id: int, number_in_list: int) -> None:
        """
        Removes entry from favourite list by its position
        If favourites list does not exist, does nothing
        Raise ValueError if number is incorrect
        """
        ...


class FavouriteUsersListsService:
    def __init__(self, fave_repo: IFavouriteUsersListsRepository, user_service: UserService):
        self._fave_repo = fave_repo
        self._user_service = user_service

    def list(self, telegram_id: int) -> Tuple[TelegramBotUserInfo, ...]:
        if not self._user_service.is_phone_number_set(telegram_id):
            raise PhoneNumberNotSet()

        return self._fave_repo.list(telegram_id)

    @sync_to_async
    def alist(self, telegram_id: int) -> Tuple[TelegramBotUserInfo, ...]:
        return self.list(telegram_id)

    def add(self, owner_telegram_id: int, add_telegram_id: int) -> None:
        if not self._user_service.is_phone_number_set(owner_telegram_id):
            raise PhoneNumberNotSet()

        return self._fave_repo.add(owner_telegram_id, add_telegram_id)

    @sync_to_async
    def aadd(self, owner_telegram_id: int, add_telegram_id: int) -> None:
        return self.add(owner_telegram_id, add_telegram_id)

    def add_by_username(self, owner_telegram_id: int, username: str):
        if not self._user_service.is_phone_number_set(owner_telegram_id):
            raise PhoneNumberNotSet()

        if username is None:
            raise NotFoundException()

        entity = self._user_service.get_by_username(username)
        if entity is None:
            raise NotFoundException()

        return self._fave_repo.add(owner_telegram_id, entity.telegram_id)

    def delete(self, telegram_id: int, deletable_telegram_id: int) -> None:
        if not self._user_service.is_phone_number_set(telegram_id):
            raise PhoneNumberNotSet()

        return self._fave_repo.delete(telegram_id, deletable_telegram_id)

    def delete_by_username(self, telegram_id: int, username: str) -> None:
        if not self._user_service.is_phone_number_set(telegram_id):
            raise PhoneNumberNotSet()

        if username is None:
            raise NotFoundException()

        entity = self._user_service.get_by_username(username)
        if entity is None:
            raise NotFoundException()

        return self._fave_repo.delete(telegram_id, entity.telegram_id)

    def delete_by_number_in_list(self, telegram_id: int, number_in_list: int) -> None:
        """
        Removes entry from favourite list by its position
        If favourites list does not exist, does nothing
        Raise ValueError if number is incorrect
        """
        if not self._user_service.is_phone_number_set(telegram_id):
            raise PhoneNumberNotSet()

        return self._fave_repo.delete_by_number_in_list(telegram_id, number_in_list)

    @sync_to_async
    def adelete_by_number_in_list(self, telegram_id: int, number_in_list: int) -> None:
        return self.delete_by_number_in_list(telegram_id, number_in_list)
