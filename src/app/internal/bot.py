from functools import reduce
from typing import Tuple

from telegram.ext import Application, CommandHandler, ConversationHandler, MessageHandler, filters

import app.internal.bank_accounts.bot
import app.internal.bank_cards.bot
import app.internal.bank_transactions.bot
import app.internal.common.bot
import app.internal.common.presentation.bot.states
import app.internal.favourite_users_lists.bot
import app.internal.users.bot
import app.internal.users.presentation.bot.commands
import app.internal.users.presentation.bot.states
import app.internal.users.presentation.bot.wrappers
from app.internal.bank_accounts.db.repositories import BankAccountRepository, BankExchangeRateRepository
from app.internal.bank_accounts.domain.services import BankAccountService, BankExchangeRateService
from app.internal.bank_cards.db.repositories import BankCardRepository
from app.internal.bank_cards.domain.services import BankCardService
from app.internal.bank_transactions.domain.services import BankStatisticsService, BankTransactionService
from app.internal.common.domain.bot_persistence_service import TelegramBotPersistenceService
from app.internal.common.presentation.bot.types import StateSettingCallback
from app.internal.favourite_users_lists.db.repositories import FavouriteUsersListsRepository
from app.internal.favourite_users_lists.domain.services import FavouriteUsersListsService
from app.internal.users.db.repositories import UserRepository
from app.internal.users.domain.services import UserService


def apply_wrappers(wrappers: Tuple[StateSettingCallback, ...]):
    def inner(handler: StateSettingCallback):
        result = handler
        for wrapper in wrappers:
            result = wrapper(result)
        return result

    return inner


def dependencies_injection():
    user_repo = UserRepository()
    user_service = UserService(user_repo)

    accounts_repo = BankAccountRepository()
    accounts_service = BankAccountService(accounts_repo, user_service)

    cards_repo = BankCardRepository()
    cards_service = BankCardService(cards_repo, user_service)

    favourites_repo = FavouriteUsersListsRepository()
    favourites_service = FavouriteUsersListsService(favourites_repo, user_service)

    exchange_rate_repo = BankExchangeRateRepository()
    exchange_rate_service = BankExchangeRateService(exchange_rate_repo)

    transaction_service = BankTransactionService(exchange_rate_service)

    statistics_service = BankStatisticsService(transaction_service)

    persistence_service = TelegramBotPersistenceService(user_service, transaction_service)

    build_points = [
        app.internal.common.bot.build_handlers(persistence_service),
        app.internal.users.bot.build_handlers(user_service),
        app.internal.bank_accounts.bot.build_handlers(accounts_service),
        app.internal.bank_cards.bot.build_handlers(cards_service),
        app.internal.favourite_users_lists.bot.build_handlers(favourites_service, user_service),
        app.internal.bank_transactions.bot.build_handlers(
            transaction_service,
            statistics_service,
            persistence_service,
            user_service,
            favourites_service,
            accounts_service,
            cards_service,
        ),
    ]

    bot_wrappers = app.internal.users.presentation.bot.wrappers.BotWrappers(user_service)

    return build_points, bot_wrappers


def build_handlers():
    build_points, bot_wrappers = dependencies_injection()

    wrappers_active = (bot_wrappers.telegrambotuser_update_username_wrapper,)
    wrap_func = apply_wrappers(wrappers_active)

    command_handlers = []
    states = {}
    for c, s in build_points:
        command_handlers += c
        states |= s

    command_handlers = list(
        map(
            lambda handler: CommandHandler(
                handler.commands, wrap_func(handler.callback), handler.filters, handler.block
            ),
            command_handlers,
        )
    )

    (command_handler_cancel,) = (h for h in command_handlers if "cancel" in h.commands)
    command_not_found_handler = app.internal.common.bot.get_command_not_found_handler(command_handlers, states)

    states = reduce(
        lambda dict1, dict2: {**dict1, **dict2},
        map(
            lambda x: {  # x[0] - state, x[1] - handler
                x[0]: [
                    command_handler_cancel,
                    MessageHandler(filters.BaseFilter(), wrap_func(x[1])),
                ]
            },
            states.items(),
        ),
    )

    return command_handlers, states, command_not_found_handler


class TelegramBot:
    @staticmethod
    def setup_handlers(application: Application):
        command_handlers, states, command_not_found_handler = build_handlers()

        conversation_handler = ConversationHandler(
            entry_points=command_handlers + [command_not_found_handler],
            states=states,
            fallbacks=[command_not_found_handler],
        )
        application.add_handler(conversation_handler)

    def __init__(self, token: str):
        """
        Telegram bot build procedure
        """
        self._token = token

        application = Application.builder().token(self._token).build()
        self.setup_handlers(application)

        self._application = application

    def start_polling(self):
        self._application.run_polling()

    def start_webhook(self, host: str, port: int, webhook_url: str):
        self._application.run_webhook(host, port, "/telegram", None, None, 10, webhook_url)
