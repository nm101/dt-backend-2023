from typing import Tuple

from .bank_accounts.db.repositories import BankAccountRepository, BankExchangeRateRepository
from .bank_accounts.domain.services import BankAccountService, BankExchangeRateService
from .bank_accounts.presentation.monitoring import BankAccountsMonitoringFactory
from .bank_transactions.domain.services import BankTransactionService
from .bank_transactions.presentation.monitoring import BankTransactionMonitoringFactory
from .common.presentation.monitoring import IMonitoringFactory
from .users.db.repositories import UserRepository
from .users.domain.services import UserService
from .users.presentation.monitoring import UsersMonitoringFactory

user_repo = UserRepository()
user_service = UserService(user_repo)
account_repo = BankAccountRepository()
account_service = BankAccountService(account_repo, user_service)
exchange_rate_repo = BankExchangeRateRepository()
exchange_rate_service = BankExchangeRateService(exchange_rate_repo)
transaction_service = BankTransactionService(exchange_rate_service)

monitoring_factories: Tuple[IMonitoringFactory, ...] = (
    UsersMonitoringFactory(user_service),
    BankAccountsMonitoringFactory(account_service),
    BankTransactionMonitoringFactory(transaction_service),
)

for factory in monitoring_factories:
    factory.build_metrics()
