from ninja_extra import NinjaExtraAPI
from ninja_jwt.controller import NinjaJWTDefaultController

from app.internal.bank_accounts.db.repositories import BankAccountRepository
from app.internal.bank_accounts.domain.services import BankAccountService
from app.internal.bank_accounts.presentation.handlers import BankAccountHandlers
from app.internal.bank_accounts.presentation.routers import add_accounts_router
from app.internal.bank_cards.db.repositories import BankCardRepository
from app.internal.bank_cards.domain.services import BankCardService
from app.internal.bank_cards.presentation.handlers import BankCardHandlers
from app.internal.bank_cards.presentation.routers import add_cards_router
from app.internal.common.domain.entities import ErrorResponse
from app.internal.common.domain.exceptions import NotFoundException
from app.internal.common.presentation import text_lines_constants as text_lines_constants_common
from app.internal.favourite_users_lists.db.repositories import FavouriteUsersListsRepository
from app.internal.favourite_users_lists.domain.services import FavouriteUsersListsService
from app.internal.favourite_users_lists.presentation.handlers import FavouritesHandlers
from app.internal.favourite_users_lists.presentation.routers import add_favourites_router
from app.internal.users.db.repositories import UserRepository
from app.internal.users.domain.exceptions import InvalidPassword, InvalidPhoneNumber, PhoneNumberNotSet
from app.internal.users.domain.services import UserService
from app.internal.users.presentation import text_lines_constants
from app.internal.users.presentation.handlers import UserHandlers
from app.internal.users.presentation.routers import add_users_router

from . import monitoring


def get_api() -> NinjaExtraAPI:
    api = NinjaExtraAPI(
        title="DT.EDU.BACKEND",
        version="1.0.0",
    )

    # Django Ninja JWT
    api.register_controllers(NinjaJWTDefaultController)

    @api.exception_handler(PhoneNumberNotSet)
    def phone_number_not_set(request, exc):
        return api.create_response(
            request,
            ErrorResponse(message=text_lines_constants.general_command_phone_number_not_set_error_message),
            status=403,
        )

    @api.exception_handler(InvalidPhoneNumber)
    def invalid_phone_number(request, exc):
        return api.create_response(
            request,
            ErrorResponse(message=text_lines_constants.state_waiting_phone_number_validation_error_text),
            status=422,
        )

    @api.exception_handler(InvalidPassword)
    def invalid_password(request, exc):
        return api.create_response(
            request,
            ErrorResponse(message=text_lines_constants.invalid_password),
            status=422,
        )

    @api.exception_handler(NotFoundException)
    def not_found(request, exc):
        return api.create_response(
            request, ErrorResponse(message=text_lines_constants_common.rest_message_not_found), status=404
        )

    user_repo = UserRepository()
    user_service = UserService(user_repo)

    favourites_repo = FavouriteUsersListsRepository()
    favourites_service = FavouriteUsersListsService(favourites_repo, user_service)

    accounts_repo = BankAccountRepository()
    accounts_service = BankAccountService(accounts_repo, user_service)

    cards_repo = BankCardRepository()
    cards_service = BankCardService(cards_repo, user_service)

    users_handlers = UserHandlers(user_service)
    add_users_router(api, users_handlers)

    favourites_handlers = FavouritesHandlers(favourites_service)
    add_favourites_router(api, favourites_handlers)

    accounts_handlers = BankAccountHandlers(accounts_service)
    add_accounts_router(api, accounts_handlers)

    cards_handlers = BankCardHandlers(cards_service)
    add_cards_router(api, cards_handlers)

    return api


ninja_api = get_api()
