from typing import Optional, Tuple

from app.internal.bank_cards.db.models import BankCard
from app.internal.bank_cards.domain.entities import BankCardInfo
from app.internal.bank_cards.domain.services import IBankCardRepository


class BankCardRepository(IBankCardRepository):
    def get(self, card_number: int) -> Optional[BankCardInfo]:
        """
        Retrieve bank card info by account ID
        Returns None if card not exists
        """

        obj = BankCard.objects.filter(number=card_number).first()
        if not obj:
            return
        return BankCardInfo.from_orm(obj)

    def list(self, telegram_id: int) -> Tuple[BankCardInfo, ...]:
        queryset = BankCard.objects.filter(owner_id=telegram_id).all()
        return tuple(BankCardInfo.from_orm(entity) for entity in queryset)
