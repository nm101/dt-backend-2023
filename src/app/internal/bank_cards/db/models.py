from django.core.validators import RegexValidator
from django.db import models

from app.internal.bank_accounts.db.models import BankAccount
from app.internal.bank_cards.domain import model_functions
from app.internal.users.db.models import TelegramBotUser


class BankCard(models.Model):
    number = models.PositiveBigIntegerField(
        primary_key=True, editable=False, default=model_functions.generate_card_number
    )
    expiration_month = models.DateField()

    cvc_code_sha256 = models.CharField(
        verbose_name="CVC code", max_length=88, validators=[RegexValidator(regex=r"^\d{3}$")]
    )

    owner = models.ForeignKey(to=TelegramBotUser, on_delete=models.PROTECT)
    account = models.ForeignKey(to=BankAccount, on_delete=models.PROTECT)

    def __str__(self):
        number_displayable = model_functions.number_displayable(self.number)
        return f"Card {number_displayable}"

    def save(self, *args, **kwargs):
        if self._state.adding:
            model_functions.on_create(self)

        return super().save(*args, **kwargs)
