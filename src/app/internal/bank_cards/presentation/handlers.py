from typing import List

from django.http import HttpRequest

from app.internal.bank_cards.domain.entities import BankCardInfo
from app.internal.bank_cards.domain.services import BankCardService


class BankCardHandlers:
    def __init__(self, card_service: BankCardService):
        self._card_service = card_service

    def list(self, request: HttpRequest) -> List[BankCardInfo]:
        telegram_id = request.user.telegram_id

        return list(self._card_service.list(telegram_id))
