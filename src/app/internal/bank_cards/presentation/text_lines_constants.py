from typing import Optional, Tuple

from app.internal.bank_cards.domain.entities import BankCardInfo


def bot_cards_list_response_template(
    card: BankCardInfo, displaying_to_owner: bool = True, numbering_number: Optional[int] = None
):
    numbering_str = f"{numbering_number}) " if numbering_number is not None else ""
    return (
        (
            numbering_str + f"Number {card.number_displayable}:\n"
            f"  Account ID: {card.account_id}\n"
            f"  Balance: {card.balance_displayable}\n"
        )
        if displaying_to_owner
        else (numbering_str + f"Number {card.number_displayable_public}:\n" f"  Currency: {card.currency_str}")
    )


def general_cards_list(
    cards: Tuple[BankCardInfo, ...], title: str, numbered: bool = False, displaying_to_owner: bool = True
) -> str:
    count = [0]

    def map_func(card):
        count[0] += 1
        numbering_number = count[0] if numbered else None
        return bot_cards_list_response_template(card, displaying_to_owner, numbering_number)

    return "\n".join((f"{title}\n", "\n".join(map(map_func, cards))))


command_cards_list_title = "Cards available:"
general_user_have_no_bank_cards = "You don't have any bank cards yet"
