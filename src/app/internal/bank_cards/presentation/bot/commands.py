from telegram import Update
from telegram.ext import ContextTypes

import app.internal.bank_cards.presentation.text_lines_constants
import app.internal.users.presentation.text_lines_constants
from app.internal.bank_cards.domain.services import BankCardService
from app.internal.users.domain.exceptions import PhoneNumberNotSet


class BotHandlers:
    def __init__(self, cards_service: BankCardService):
        self._cards_service = cards_service

    async def cards_list(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        """
        Displays all bank cards owned by user
        """
        telegram_id = update.message.from_user.id

        try:
            cards = await self._cards_service.alist(telegram_id)

            if cards:
                response_message = app.internal.bank_cards.presentation.text_lines_constants.general_cards_list(
                    cards, title=app.internal.bank_cards.presentation.text_lines_constants.command_cards_list_title
                )
            else:
                response_message = (
                    app.internal.bank_cards.presentation.text_lines_constants.general_user_have_no_bank_cards
                )
        except PhoneNumberNotSet:
            response_message = (
                app.internal.users.presentation.text_lines_constants.general_command_phone_number_not_set_error_message
            )

        await context.bot.send_message(update.message.chat_id, response_message)
