from typing import List

from ninja_extra import NinjaExtraAPI, Router
from ninja_jwt.authentication import JWTAuth

from app.internal.bank_cards.domain.entities import BankCardInfo
from app.internal.bank_cards.presentation.handlers import BankCardHandlers
from app.internal.common.domain.entities import ErrorResponse


def get_cards_router(card_handlers: BankCardHandlers) -> Router:
    router = Router(tags=["cards"])

    router.add_api_operation(
        "",
        ["GET"],
        card_handlers.list,
        url_name="cards_list",
        response={200: List[BankCardInfo], 403: ErrorResponse},
        auth=JWTAuth(),
    )

    return router


def add_cards_router(api: NinjaExtraAPI, cards_handlers: BankCardHandlers):
    cards_handler = get_cards_router(cards_handlers)
    api.add_router("/cards", cards_handler, tags=cards_handler.tags)
