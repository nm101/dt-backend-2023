from django.contrib import admin
from django.contrib.admin import ModelAdmin

from app.internal.bank_cards.db.models import BankCard


@admin.register(BankCard)
class BankCardAdmin(ModelAdmin):
    list_display = ("__str__", "owner", "account")

    def get_exclude(self, request, obj=None):
        if obj:
            return ("cvc_code_sha256",)
        return ()
