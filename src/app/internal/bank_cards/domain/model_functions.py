from uuid import uuid4

from django.contrib.auth import hashers


def generate_card_number() -> int:
    uuid_int = uuid4().int
    shifted = uuid_int & (1 << 64) - 1
    last_16 = shifted % (10**16)

    return last_16


def on_create(card) -> None:
    cvc_code = card.cvc_code_sha256

    # uses django default pbkdf2_sha256
    card.cvc_code_sha256 = hashers.make_password(cvc_code)


def number_displayable(card_number: int) -> str:
    number_str = str(card_number)

    # add leading zero to make it always 16 characters long
    leading_zeros_count = 16 - len(number_str)
    if leading_zeros_count > 0:
        number_str = "0" * leading_zeros_count + number_str

    return f"{number_str[0:4]} {number_str[4:8]} {number_str[8:12]} {number_str[12:16]}"


def number_displayable_public(card_number: int) -> str:
    number_last_four_str = str(card_number % 10000)
    leading_zeros_count = 4 - len(number_last_four_str)
    if leading_zeros_count > 0:
        number_last_four_str = "0" * leading_zeros_count + number_last_four_str
    return f"XXXX XXXX XXXX {number_last_four_str}"
