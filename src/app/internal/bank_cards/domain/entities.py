from __future__ import annotations

from ninja.orm import create_schema

from app.internal.bank_accounts.domain.entities import BankAccountInfo
from app.internal.bank_cards.db.models import BankCard
from app.internal.bank_cards.domain import model_functions
from app.internal.users.domain.entities import TelegramBotUserInfo

BankCardSchema = create_schema(
    BankCard,
    depth=2,
    fields=["number", "expiration_month", "owner", "account"],
    custom_fields=[("account", BankAccountInfo, None), ("owner", TelegramBotUserInfo, None)],
)


class BankCardInfo(BankCardSchema):
    @property
    def number_displayable(self):
        return model_functions.number_displayable(self.number)

    @property
    def number_displayable_public(self):
        return model_functions.number_displayable_public(self.number)

    @property
    def account_id(self):
        return self.account.id

    @property
    def balance(self):
        return self.account.balance

    @property
    def balance_displayable(self):
        return self.account.balance_displayable

    @property
    def currency_str(self):
        return self.account.currency_str

    @property
    def owner_id(self):
        return self.owner.telegram_id
