import re
from abc import ABC
from typing import Optional, Tuple

from asgiref.sync import sync_to_async

from app.internal.bank_cards.domain.entities import BankCardInfo
from app.internal.users.domain.exceptions import PhoneNumberNotSet
from app.internal.users.domain.services import UserService

card_number_str_regex = re.compile(r"^(\d{4} ){3}\d{4}$")


class IBankCardRepository(ABC):
    def get(self, card_number: int) -> Optional[BankCardInfo]:
        """
        Retrieve bank card info by account ID
        Returns None if card not exists
        """
        ...

    def list(self, telegram_id: int) -> Tuple[BankCardInfo, ...]:
        ...


class BankCardService:
    """
    Bank cards service
    """

    def __init__(self, card_repo: IBankCardRepository, user_service: UserService):
        self._card_repo = card_repo
        self._user_service = user_service

    @staticmethod
    def is_valid_number_str(number_str: str) -> bool:
        return bool(card_number_str_regex.match(number_str))

    def get(self, card_number: int) -> Optional[BankCardInfo]:
        """
        Retrieve bank card info by account ID
        Returns None if card not exists
        """
        return self._card_repo.get(card_number)

    @sync_to_async
    def aget(self, card_number: int) -> BankCardInfo:
        return self.get(card_number)

    def list(self, telegram_id: int) -> Tuple[BankCardInfo, ...]:
        if not self._user_service.is_phone_number_set(telegram_id):
            raise PhoneNumberNotSet()

        return self._card_repo.list(telegram_id)

    @sync_to_async
    def alist(self, telegram_id: int) -> Tuple[BankCardInfo, ...]:
        return self.list(telegram_id)
