from typing import Dict, List, Tuple

from telegram.ext import CommandHandler

from .domain.services import BankCardService
from .presentation.bot import commands


def build_handlers(cards_service: BankCardService) -> Tuple[List, Dict]:
    command_handlers_class = commands.BotHandlers(cards_service)

    command_handlers = [
        CommandHandler("cards_list", command_handlers_class.cards_list),
    ]

    state_handlers = {}

    return command_handlers, state_handlers
