from ninja import Schema
from ninja.orm import create_schema
from pydantic import Field

from app.internal.users.db.models import TelegramBotUser

TelegramBotUserInfo = create_schema(TelegramBotUser, fields=["telegram_id", "username", "phone_number"], name="UserOut")
TelegramBotUserSelectorUsername = create_schema(TelegramBotUser, fields=["username"], name="UserSelectorUsername")
TelegramBotUserSelectorPhone = create_schema(TelegramBotUser, fields=["phone_number"], name="UserSelectorPhoneNumber")


class TelegramBotUserChangePassword(Schema):
    old_password: str = Field(max_length=255)
    new_password: str = Field(max_length=255)
