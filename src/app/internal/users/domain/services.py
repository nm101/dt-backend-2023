from abc import ABC
from typing import Optional

from asgiref.sync import sync_to_async
from django.core.exceptions import ValidationError
from phonenumber_field import validators as phone_number_validator
from phonenumber_field.validators import validate_international_phonenumber

from app.internal.users.domain.entities import TelegramBotUserInfo
from app.internal.users.domain.exceptions import InvalidPassword, InvalidPhoneNumber, PhoneNumberNotSet


class IUserRepository(ABC):
    def get_by_username(self, username: str) -> Optional[TelegramBotUserInfo]:
        """
        Retrieve user by username
        Returns None if not found
        """
        ...

    def try_create(self, telegram_id: int) -> bool:
        """
        Tries to create ORM entity.
        Returns flag whether entity was created or not.
        """

        ...

    def try_update_username(self, telegram_id: int, username: Optional[str]) -> bool:
        """
        Tries to update username field
        Returns boolean whether succeeded or not
        """

        ...

    def exists(self, telegram_id: int) -> bool:
        ...

    def set_phone_number(self, telegram_id: int, phone_number: str) -> None:
        """
        Sets a phone number to specified TelegramBotUser
        """

        ...

    def get_user(self, telegram_id: int) -> Optional[TelegramBotUserInfo]:
        """
        Returns information about user, if exists.
        If user does not exist, returns None.
        """

        ...

    def check_password(self, telegram_id: int, password: Optional[str]) -> bool:
        ...

    def change_password(self, telegram_id: int, new_password: str) -> None:
        ...

    def get_users_count(self) -> int:
        ...


class UserService:
    def __init__(self, repository: IUserRepository):
        self._repository = repository

    def get_users_count(self) -> int:
        return self._repository.get_users_count()

    def get_by_username(self, username: str) -> Optional[TelegramBotUserInfo]:
        """
        Retrieve user information by username
        Returns none if not found
        """
        return self._repository.get_by_username(username)

    @sync_to_async
    def aget_by_username(self, username: str) -> Optional[TelegramBotUserInfo]:
        return self.get_by_username(username)

    def _is_phone_number_set(self, entity: TelegramBotUserInfo):
        try:
            validate_international_phonenumber(entity.phone_number)
        except ValidationError:
            return False
        return bool(entity.phone_number)

    def is_phone_number_set(self, telegram_id: int) -> bool:
        entity = self._repository.get_user(telegram_id)
        if entity is None:
            return False
        return self._is_phone_number_set(entity)

    @sync_to_async
    def ais_phone_number_set(self, telegram_id: int) -> bool:
        return self.is_phone_number_set(telegram_id)

    def try_create(self, telegram_id: int) -> bool:
        """
        Tries to create user.
        Returns flag whether it was created or not.
        """
        return self._repository.try_create(telegram_id)

    @sync_to_async
    def atry_create(self, telegram_id: int) -> bool:
        """
        Async version of try_create
        """
        return self.try_create(telegram_id)

    def try_update_username(self, telegram_id: int, username: Optional[str]) -> bool:
        """
        Tries to update user's username
        Returns boolean whether succeeded or not
        """

        return self._repository.try_update_username(telegram_id, username)

    @sync_to_async
    def atry_update_username(self, telegram_id: int, username: Optional[str]) -> None:
        """
        Async version of try_update_username
        """
        self.try_update_username(telegram_id, username)

    def exists(self, telegram_id: int) -> bool:
        return self._repository.exists(telegram_id)

    def set_phone_number(self, telegram_id: int, phone_number: str) -> None:
        """
        Validates and sets a phone number to specified TelegramBotUser
        Raises django ValidationError if validation fails
        """
        try:
            phone_number_validator.validate_international_phonenumber(phone_number)
        except ValidationError:
            raise InvalidPhoneNumber()
        self._repository.set_phone_number(telegram_id, phone_number)

    @sync_to_async
    def aset_phone_number(self, telegram_id: int, phone_number: str) -> None:
        return self.set_phone_number(telegram_id, phone_number)

    def get_me(self, telegram_id: int) -> Optional[TelegramBotUserInfo]:
        """
        Returns information about user, if exists.
        If user does not exist, returns None.
        Requires user to have phone number set, otherwise raises PhoneNumberNotSet.
        """
        entity = self._repository.get_user(telegram_id)
        if entity is None:
            return
        if not self._is_phone_number_set(entity):
            raise PhoneNumberNotSet()

        return entity

    @sync_to_async
    def aget_me(self, telegram_id: int) -> Optional[TelegramBotUserInfo]:
        return self.get_me(telegram_id)

    def change_password(self, telegram_id: int, old_password: Optional[str], new_password: str):
        if not self._repository.check_password(telegram_id, old_password):
            raise InvalidPassword()

        self._repository.change_password(telegram_id, new_password)
