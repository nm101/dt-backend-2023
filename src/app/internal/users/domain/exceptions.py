class PhoneNumberNotSet(Exception):
    pass


class InvalidPhoneNumber(Exception):
    pass


class InvalidPassword(Exception):
    pass
