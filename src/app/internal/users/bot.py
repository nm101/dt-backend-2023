from typing import Dict, List, Tuple

from telegram.ext import CommandHandler

from app.internal.common.domain.conversation_state import ConversationState as State

from .domain.services import UserService
from .presentation.bot import commands, states


def build_handlers(user_service: UserService) -> Tuple[List, Dict]:
    command_handlers_class = commands.BotHandlers(user_service)
    state_handlers_class = states.BotHandlers(user_service)

    command_handlers = [
        CommandHandler("start", command_handlers_class.start),
        CommandHandler("set_phone", command_handlers_class.set_phone),
        CommandHandler("me", command_handlers_class.me),
    ]

    state_handlers = {
        State.SetPhone_AwaitingPhoneNumber: state_handlers_class.awaiting_phone_number,
    }

    return command_handlers, state_handlers
