from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _
from phonenumber_field.validators import validate_international_phonenumber


class TelegramBotUserManager(BaseUserManager):
    """
    Custom user model manager where telegram ID is the unique identifiers
    for authentication instead of usernames.
    """

    def create_user(self, telegram_id, password, **extra_fields):
        """
        Create and save a user with the given telegram ID and password.
        """
        if not telegram_id:
            raise ValueError(_("The telegram ID must be set"))

        user = self.model(telegram_id=telegram_id, **extra_fields)

        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, telegram_id, password, **extra_fields):
        """
        Create and save a SuperUser with the given telegram ID and password.
        """
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError(_("Superuser must have is_staff=True."))
        if extra_fields.get("is_superuser") is not True:
            raise ValueError(_("Superuser must have is_superuser=True."))
        return self.create_user(telegram_id, password, **extra_fields)


class TelegramBotUser(AbstractUser):
    telegram_id = models.BigIntegerField(primary_key=True, verbose_name="Telegram ID")
    username = models.CharField(max_length=32, null=True, blank=True)
    phone_number = models.CharField(
        max_length=32, null=True, blank=True, validators=[validate_international_phonenumber]
    )

    first_name = None
    last_name = None
    email = None

    USERNAME_FIELD = "telegram_id"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = "Telegram bot user"

    objects = TelegramBotUserManager()

    def __str__(self):
        return f"@{self.username} (ID {self.telegram_id})" if self.username else f"ID {self.telegram_id}"
