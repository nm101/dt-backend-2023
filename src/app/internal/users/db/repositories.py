from typing import Optional

from app.internal.common.domain.exceptions import NotFoundException
from app.internal.users.db.models import TelegramBotUser
from app.internal.users.domain.entities import TelegramBotUserInfo
from app.internal.users.domain.services import IUserRepository


class UserRepository(IUserRepository):
    def get_users_count(self) -> int:
        return TelegramBotUser.objects.count()

    def get_by_username(self, username: str) -> Optional[TelegramBotUserInfo]:
        """
        Retrieve user information by username
        Returns none if not found
        """
        entity = TelegramBotUser.objects.filter(username=username).first()
        if not entity:
            return None
        return TelegramBotUserInfo.from_orm(entity)

    def __init__(self):
        ...

    def _try_get_orm_entity(self, telegram_id: int) -> Optional[TelegramBotUser]:
        """
        Internal method.
        Retrieves ORM entity without raising an exception if it's not exists
        Returns either TelegramBotUser entity either None if entity doesn't exist
        """
        return TelegramBotUser.objects.filter(telegram_id=telegram_id).first()

    def try_create(self, telegram_id: int) -> bool:
        """
        Tries to create ORM entity.
        Returns flag whether entity was created or not.
        """
        orm_entity, created = TelegramBotUser.objects.get_or_create(telegram_id=telegram_id)
        return created

    def try_update_username(self, telegram_id: int, username: Optional[str]) -> bool:
        """
        Tries to update username field
        Returns boolean whether succeeded or not
        """

        orm_entity = self._try_get_orm_entity(telegram_id)
        if not orm_entity:
            return False

        orm_entity.username = username
        orm_entity.save()
        return True

    def exists(self, telegram_id: int) -> bool:
        return self._try_get_orm_entity(telegram_id) is not None

    def set_phone_number(self, telegram_id: int, phone_number: str) -> None:
        """
        Sets a phone number to specified TelegramBotUser
        """

        TelegramBotUser.objects.update_or_create(telegram_id=telegram_id, defaults={"phone_number": phone_number})

    def get_user(self, telegram_id: int) -> Optional[TelegramBotUserInfo]:
        """
        Returns information about user, if exists.
        If user does not exist, returns None.
        """
        orm_entity = self._try_get_orm_entity(telegram_id)

        if orm_entity is None:
            return None

        return TelegramBotUserInfo.from_orm(orm_entity)

    def _check_password(self, entity: TelegramBotUser, password: Optional[str]) -> bool:
        if entity is None:
            raise NotFoundException()

        if not bool(entity.password):
            return True

        return entity.check_password(password)

    def check_password(self, telegram_id: int, password: Optional[str]) -> bool:
        entity = self._try_get_orm_entity(telegram_id)
        return self._check_password(entity, password)

    def change_password(self, telegram_id: int, new_password: str) -> None:
        entity = self._try_get_orm_entity(telegram_id)
        if entity is None:
            raise NotFoundException()

        entity.set_password(new_password)
        entity.save()
