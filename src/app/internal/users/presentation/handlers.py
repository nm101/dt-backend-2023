from django.http import HttpRequest

from app.internal.users.domain.entities import (
    TelegramBotUserChangePassword,
    TelegramBotUserInfo,
    TelegramBotUserSelectorPhone,
)
from app.internal.users.domain.services import UserService


class UserHandlers:
    def __init__(self, user_service: UserService):
        self._user_service = user_service

    def me(self, request: HttpRequest) -> TelegramBotUserInfo:
        telegram_id = request.user.telegram_id

        return self._user_service.get_me(telegram_id)

    def set_phone(self, request: HttpRequest, body: TelegramBotUserSelectorPhone):
        telegram_id = request.user.telegram_id

        self._user_service.set_phone_number(telegram_id, body.phone_number)
        return body

    def change_password(self, request: HttpRequest, body: TelegramBotUserChangePassword):
        telegram_id = request.user.telegram_id

        self._user_service.change_password(telegram_id, body.old_password, body.new_password)
