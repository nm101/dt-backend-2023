from functools import wraps
from typing import Optional

from telegram import Update
from telegram.ext import ContextTypes

from app.internal.common.domain.conversation_state import ConversationState
from app.internal.common.presentation.bot.types import StateSettingCallback
from app.internal.users.domain.services import UserService


class BotWrappers:
    def __init__(self, user_service: UserService):
        self._user_service = user_service

    async def _update_username(self, update: Update) -> bool:
        telegram_user = update.message.from_user

        return await self._user_service.atry_update_username(telegram_user.id, telegram_user.username)

    def telegrambotuser_update_username_wrapper(self, callback: StateSettingCallback):
        @wraps(callback)
        async def inner(update: Update, context: ContextTypes.DEFAULT_TYPE) -> Optional[ConversationState]:
            updated_before_call = await self._update_username(update)

            state = await callback(update, context)

            if not updated_before_call:
                await self._update_username(update)

            return state

        return inner
