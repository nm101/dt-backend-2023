from typing import Optional

from django.core.exceptions import ValidationError
from telegram import Update
from telegram.ext import ContextTypes

import app.internal.users.presentation.text_lines_constants
from app.internal.common.domain.conversation_state import ConversationState
from app.internal.users.domain.exceptions import InvalidPhoneNumber
from app.internal.users.domain.services import UserService


class BotHandlers:
    def __init__(self, user_service: UserService):
        self._user_service = user_service

    async def awaiting_phone_number(
        self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ) -> Optional[ConversationState]:
        telegram_id = update.message.from_user.id
        phone_number = update.message.text
        try:
            await self._user_service.aset_phone_number(telegram_id, phone_number)
        except InvalidPhoneNumber:
            await context.bot.send_message(
                update.message.chat_id,
                app.internal.users.presentation.text_lines_constants.state_waiting_phone_number_validation_error_text,
            )
            return
        else:
            await context.bot.send_message(
                update.message.chat_id,
                app.internal.users.presentation.text_lines_constants.state_waiting_phone_number_success_text,
            )
            return ConversationState.Start
