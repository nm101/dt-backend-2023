from asgiref.sync import sync_to_async
from telegram import Update
from telegram.ext import ContextTypes

import app.internal.users.presentation.text_lines_constants
from app.internal.common.domain.conversation_state import ConversationState
from app.internal.users.domain.exceptions import PhoneNumberNotSet
from app.internal.users.domain.services import UserService


class BotHandlers:
    def __init__(self, user_service: UserService):
        self._user_service = user_service

    async def start(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        """
        /start command callback
        """

        telegram_user = update.message.from_user

        await self._user_service.atry_create(telegram_user.id)

        welcome_text = app.internal.users.presentation.text_lines_constants.command_start_welcome_text(telegram_user)
        await context.bot.send_message(update.message.chat_id, welcome_text)

    async def me(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        """
        Show information about user
        """

        try:
            user_info = await self._user_service.aget_me(update.message.from_user.id)
            response_message = app.internal.users.presentation.text_lines_constants.command_me_user_information_text(
                user_info
            )
        except PhoneNumberNotSet:
            response_message = (
                app.internal.users.presentation.text_lines_constants.general_command_phone_number_not_set_error_message
            )

        await context.bot.send_message(update.message.chat_id, response_message)

    async def set_phone(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> ConversationState:
        """Set phone number"""
        await context.bot.send_message(
            update.message.chat_id, app.internal.users.presentation.text_lines_constants.command_set_phone_text
        )
        return ConversationState.SetPhone_AwaitingPhoneNumber
