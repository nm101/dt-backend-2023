from typing import Optional

from telegram import Update
from telegram.ext import ContextTypes

import app.internal.users.presentation.text_lines_constants
from app.internal.common.presentation import text_lines_constants
from app.internal.users.domain.entities import TelegramBotUserInfo
from app.internal.users.domain.services import UserService


async def parse_choice_digit(update: Update, context: ContextTypes.DEFAULT_TYPE, max_choice: int) -> Optional[int]:
    response = update.message.text
    try:
        response_digit = int(response.strip())
    except ValueError:
        response_digit = None
    if response_digit is None or response_digit not in range(1, 1 + max_choice):
        await context.bot.send_message(
            update.message.chat_id,
            text_lines_constants.general_incorrect_response_numeric_range_expected(1, max_choice),
        )
        return
    return response_digit


async def parse_username_retrieve_user(
    update: Update, context: ContextTypes.DEFAULT_TYPE, user_service: UserService
) -> Optional[TelegramBotUserInfo]:
    response = update.message.text.strip()

    if response[0] == "@":
        response = response[1:]

    user = await user_service.aget_by_username(response)
    if not user:
        await context.bot.send_message(
            update.message.chat_id, app.internal.users.presentation.text_lines_constants.general_user_not_found_text
        )
        return

    return user
