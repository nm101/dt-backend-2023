from typing import Tuple

from prometheus_client import Gauge
from prometheus_client.metrics import MetricWrapperBase

from app.internal.common.presentation.monitoring import IMonitoringFactory
from app.internal.users.domain.services import UserService


class UsersMonitoringFactory(IMonitoringFactory):
    def __init__(self, user_service: UserService):
        self._user_service = user_service

    def _get_users_count(self) -> int:
        return self._user_service.get_users_count()

    def build_metrics(self) -> Tuple[MetricWrapperBase]:
        users_count = Gauge("users_count", "Number of users")
        users_count.set_function(self._get_users_count)

        return (users_count,)
