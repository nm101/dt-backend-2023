from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _

from app.internal.users.db.models import TelegramBotUser


@admin.register(TelegramBotUser)
class TelegramBotUserAdmin(UserAdmin):
    list_display = ("telegram_id", "username", "phone_number")
    fieldsets = (
        (None, {"fields": ("telegram_id", "password")}),
        (_("Personal info"), {"fields": ("username", "phone_number")}),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("telegram_id", "password1", "password2"),
            },
        ),
    )
