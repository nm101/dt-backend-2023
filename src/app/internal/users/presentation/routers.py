from ninja_extra import NinjaExtraAPI, Router
from ninja_jwt.authentication import JWTAuth

from app.internal.common.domain.entities import ErrorResponse
from app.internal.users.domain.entities import TelegramBotUserInfo, TelegramBotUserSelectorPhone
from app.internal.users.presentation.handlers import UserHandlers


def get_users_router(user_handlers: UserHandlers) -> Router:
    router = Router(tags=["users"])

    router.add_api_operation(
        "/me",
        ["GET"],
        user_handlers.me,
        url_name="me_endpoint",
        response={200: TelegramBotUserInfo, 403: ErrorResponse},
        auth=JWTAuth(),
    )

    router.add_api_operation(
        "/set_phone",
        ["POST"],
        user_handlers.set_phone,
        url_name="set_phone",
        response={200: TelegramBotUserSelectorPhone, 403: ErrorResponse, 422: ErrorResponse},
        auth=JWTAuth(),
    )

    router.add_api_operation(
        "/change_password",
        ["PUT"],
        user_handlers.change_password,
        url_name="change_password",
        response={200: None, 403: ErrorResponse},
        auth=JWTAuth(),
    )

    return router


def add_users_router(api: NinjaExtraAPI, user_handlers: UserHandlers):
    user_handler = get_users_router(user_handlers)
    api.add_router("/users", user_handler, tags=user_handler.tags)
