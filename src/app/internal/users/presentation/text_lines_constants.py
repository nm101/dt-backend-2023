from telegram import User

from app.internal.common.presentation.text_lines_constants import (
    general_help_command_suggestion,
    general_notice_cancel_command,
)
from app.internal.users.domain.entities import TelegramBotUserInfo


def command_start_welcome_text(user: User):
    return f"Welcome, {user.full_name}!\n" + general_help_command_suggestion


command_set_phone_text = (
    "Please write your phone number\n"
    "Notice: Phone number must be in an international format,\n"
    "e.g. +1 234 567 8901\n\n" + general_notice_cancel_command
)
state_waiting_phone_number_success_text = "Phone number set successfully"
state_waiting_phone_number_validation_error_text = (
    "Submitted phone number is invalid.\n" "Check it again and make sure it's written in an international format\n"
)
general_command_phone_number_not_set_error_message = (
    "Please set set your phone number via /set_phone to access this command"
)


def command_me_user_information_text(user_information: TelegramBotUserInfo):
    return (
        f"Your Telegram ID: {user_information.telegram_id}\n"
        f"Your username: {user_information.username}\n"
        f"Your phone number: {user_information.phone_number}"
    )


def general_user_displayable(user_info: TelegramBotUserInfo) -> str:
    return (
        f"@{user_info.username}" if user_info.username else f"User with ID {user_info.telegram_id} (username not set)"
    )


general_username_specify_request_text = "Please specify username"
general_user_not_found_text = "User not found, please try again"


invalid_password = "Invalid password submitted."
