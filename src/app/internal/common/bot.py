from typing import Dict, List, Tuple

from telegram.ext import CommandHandler, MessageHandler, filters

from app.internal.common.domain.conversation_state import ConversationState as State

from .domain.bot_persistence_service import TelegramBotPersistenceService
from .presentation.bot import commands, states


def build_handlers(persistence_service: TelegramBotPersistenceService) -> Tuple[List, Dict]:
    c = commands.BotHandlers(persistence_service)
    s = states.BotHandlers()

    command_handlers = [
        CommandHandler("cancel", c.cancel),
        CommandHandler("help", c.help_),
    ]

    state_handlers = {
        # command not found state
        State.Start: s.start_state_handler,
    }
    return command_handlers, state_handlers


def get_command_not_found_handler(command_handlers, state_handlers):
    handler = state_handlers[State.Start]

    return MessageHandler(filters.BaseFilter(), handler)
