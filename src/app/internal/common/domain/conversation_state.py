from enum import IntEnum

from telegram.ext import ConversationHandler


class ConversationState(IntEnum):
    Start = ConversationHandler.END
    SetPhone_AwaitingPhoneNumber = 1

    Transfer_AwaitingDestinationType = 2

    Transfer_AwaitingSourceType = 3
    Transfer_AwaitingSourceChoice = 4
    Transfer_AwaitingAmount = 5
    Transfer_AwaitingTransactionConfirmation = 6

    Transfer_ToBankAccount_AwaitingAccount = 7
    Transfer_ToBankCard_AwaitingNumber = 8

    Transfer_ToParticularTelegramUser_AwaitingUsername = 9
    Transfer_ToParticularTelegramUser_AwaitingDestinationType = 10
    Transfer_ToParticularTelegramUser_ToAccount_AwaitingChoice = 11
    Transfer_ToParticularTelegramUser_ToCard_AwaitingChoice = 12
    Transfer_ToFavouriteTelegramUser_AwaitingChoice = 13

    FavoritesList_Add_AwaitingUsername = 14
    FavoritesList_Delete_AwaitingChoice = 15

    AccountStatement_AwaitingSourceTypeChoice = 16
    AccountStatement_Card_AwaitingChoice = 17
    AccountStatement_Account_AwaitingChoice = 18

    Transfer_AwaitingChoiceAttachPostcard = 19
    Transfer_AwaitingPostcard = 20

    DEFAULT = Start
