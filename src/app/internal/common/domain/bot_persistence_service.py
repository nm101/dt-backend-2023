from typing import Dict

from asgiref.sync import sync_to_async

from app.internal.bank_transactions.db.models import BankTransaction
from app.internal.bank_transactions.domain.services import BankTransactionService
from app.internal.users.domain.exceptions import PhoneNumberNotSet
from app.internal.users.domain.services import UserService


class Persistence:
    transactions_pending: Dict[int, BankTransaction] = dict()


class TelegramBotPersistenceService:
    def __init__(self, user_service: UserService, transaction_service: BankTransactionService):
        self._user_service = user_service
        self._transaction_service = transaction_service

    def transaction_pending_reset(self, telegram_id: int) -> None:
        if not self._user_service.is_phone_number_set(telegram_id):
            raise PhoneNumberNotSet()

        transaction_pending = Persistence.transactions_pending.get(telegram_id)
        if transaction_pending is None:
            return

        Persistence.transactions_pending.pop(telegram_id)

    @sync_to_async
    def atransaction_pending_reset(self, telegram_id: int) -> None:
        return self.transaction_pending_reset(telegram_id)

    def transaction_pending_create_blank(self, telegram_id) -> None:
        if not self._user_service.is_phone_number_set(telegram_id):
            raise PhoneNumberNotSet()

        transactions_pending = Persistence.transactions_pending
        if telegram_id in transactions_pending:
            raise ValueError("Transaction is set already")

        transactions_pending[telegram_id] = self._transaction_service.create_blank(telegram_id)

    @sync_to_async
    def atransaction_pending_create_blank(self, telegram_id: int) -> None:
        return self.transaction_pending_create_blank(telegram_id)

    def transaction_pending(self, telegram_id: int) -> BankTransaction:
        if not self._user_service.is_phone_number_set(telegram_id):
            raise PhoneNumberNotSet()

        transaction_pending = Persistence.transactions_pending.get(telegram_id)
        if transaction_pending is None:
            raise KeyError("No transaction is pending on this persistence")
        return transaction_pending

    @sync_to_async
    def aget_transaction_pending(self, telegram_id: int) -> BankTransaction:
        return self.transaction_pending(telegram_id)
