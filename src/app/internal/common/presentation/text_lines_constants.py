general_help_command_suggestion = "Type /help to get list of available commands"
general_notice_cancel_command = "Notice: use /cancel anytime to cancel this conversation and use another command"

state_start_unknown_command = "Unknown command.\n" + general_help_command_suggestion

command_help_text = (
    "Available commands:\n\n"
    "/set_phone - set your phone number\n"
    "/me - get information about yourself\n"
    "\n"
    "/accounts_list - get information about your bank accounts\n"
    "/cards_list - get information about your bank cards\n"
    "/transfer - transfer money to someone\n"
    "\n"
    "/fave_list - list favourite users\n"
    "/fave_add - add user to favourites list\n"
    "/fave_del - delete user from favourites list\n"
    "\n"
    "/related_users - list users, you've been interacting with\n"
    "/account_statement - get account statement for your card or account\n"
    "/transfers_new - list new inbound transactions"
)

command_cancel_success_text = "Conversation cancelled"

general_reply_only_with_digit_choice = "Please reply with ONLY a digit of your choice"


def general_incorrect_response_numeric_range_expected(start: int, end_inclusive: int) -> str:
    return f"Incorrect response, expected number between {start} and {end_inclusive} inclusive"


rest_message_not_found = "Not found"
