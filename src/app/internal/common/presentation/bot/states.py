from telegram import Update
from telegram.ext import ContextTypes

from app.internal.common.presentation import text_lines_constants


class BotHandlers:
    def __init__(self):
        ...

    async def start_state_handler(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        """
        A callback for Start state - the first state after the user is created
        """
        await context.bot.send_message(update.message.chat_id, text_lines_constants.state_start_unknown_command)
