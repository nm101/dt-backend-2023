from typing import Optional

from telegram import Update
from telegram.ext import ContextTypes

from app.internal.common.domain.bot_persistence_service import TelegramBotPersistenceService
from app.internal.common.domain.conversation_state import ConversationState
from app.internal.common.presentation import text_lines_constants
from app.internal.users.domain.exceptions import PhoneNumberNotSet


class BotHandlers:
    def __init__(self, persistence_service: TelegramBotPersistenceService):
        self._persistence_service = persistence_service

    async def help_(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        """
        Help command
        """

        await context.bot.send_message(update.message.chat_id, text_lines_constants.command_help_text)

    async def cancel(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> Optional[ConversationState]:
        """
        Terminates current conversation
        """
        try:
            telegram_id = update.message.from_user.id

            await self._persistence_service.atransaction_pending_reset(telegram_id)
        except (PhoneNumberNotSet, KeyError):
            pass

        await context.bot.send_message(update.message.chat_id, text_lines_constants.command_cancel_success_text)
        return ConversationState.Start
