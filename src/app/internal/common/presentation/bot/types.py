from typing import Awaitable, Callable

from telegram import Update
from telegram.ext import ContextTypes

from app.internal.common.domain.conversation_state import ConversationState

GenericCallback = Callable[[Update, ContextTypes], Awaitable[None]]
StateSettingCallback = Callable[[Update, ContextTypes], Awaitable[ConversationState | None]]
