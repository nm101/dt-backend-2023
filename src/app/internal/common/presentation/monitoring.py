from abc import ABC
from typing import Tuple

from prometheus_client.metrics import MetricWrapperBase


class IMonitoringFactory(ABC):
    def build_metrics(self) -> Tuple[MetricWrapperBase]:
        ...
