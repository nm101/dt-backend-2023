from unittest.mock import call

import pytest
from telegram import MessageEntity

import app.internal.common.presentation.bot.states
import app.internal.users.presentation.bot.commands
import app.internal.users.presentation.bot.states
import app.internal.users.presentation.text_lines_constants
from app.internal.common.domain.conversation_state import ConversationState
from app.internal.common.presentation import text_lines_constants


@pytest.mark.asyncio
@pytest.mark.unit
async def test_start_state_handler_replies_unknown_command_not_alters_state(
    mocked_ptb_context, ptb_private_chat_with_user, ptb_message_update, bot_states_common
):
    mocked_update = ptb_message_update("a number nine large, a number six with extra dip", None)

    state = await bot_states_common.start_state_handler(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        ptb_private_chat_with_user.id, text_lines_constants.state_start_unknown_command
    )

    assert state is None


@pytest.mark.asyncio
@pytest.mark.unit
async def test_help_command_handler_replies_correct_not_alters_state(
    mocked_ptb_context, ptb_private_chat_with_user, ptb_message_update, bot_commands_common
):
    mocked_update = ptb_message_update("/help", MessageEntity.BOT_COMMAND)

    state = await bot_commands_common.help_(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        ptb_private_chat_with_user.id, text_lines_constants.command_help_text
    )

    assert state is None


@pytest.mark.asyncio
@pytest.mark.unit
async def test_set_phone_command_handler_replies_correct_alters_state(
    mocked_ptb_context, ptb_private_chat_with_user, ptb_message_update, bot_commands_users
):
    mocked_update = ptb_message_update("/set_phone", MessageEntity.BOT_COMMAND)
    state = await bot_commands_users.set_phone(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        ptb_private_chat_with_user.id, app.internal.users.presentation.text_lines_constants.command_set_phone_text
    )

    assert state == ConversationState.SetPhone_AwaitingPhoneNumber


@pytest.mark.asyncio
@pytest.mark.unit
async def test_set_phone_state_invalid_numbers_replies_failure_not_alters_state(
    mocked_ptb_context, ptb_private_chat_with_user, ptb_message_update, bot_states_users
):
    messages_list = ["8 800 555 35 35", "240 02 03", "+8 912 999 99 99"]

    for message_text in messages_list:
        update = ptb_message_update(message_text, None)
        state = await bot_states_users.awaiting_phone_number(update, mocked_ptb_context)

        assert state is None

    mock_call_reply_error_message = call(
        ptb_private_chat_with_user.id,
        app.internal.users.presentation.text_lines_constants.state_waiting_phone_number_validation_error_text,
    )

    mocked_ptb_context.bot.send_message.assert_has_calls(
        [mock_call_reply_error_message] * len(messages_list), any_order=True
    )
    assert len(mocked_ptb_context.mock_calls) == len(messages_list)
