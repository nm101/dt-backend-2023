from http import HTTPStatus

import pytest
from django.test.client import Client

import app.internal.users.presentation.text_lines_constants
from app.internal.common.presentation import text_lines_constants
from app.internal.users.domain.entities import TelegramBotUserInfo
from tests.conftest import deserialize_json, message_dict


@pytest.mark.skip(
    "Due to /me been rewritten in homework07 and there's no reason" " to fix it now according to homework08 roadmap"
)
@pytest.mark.unit
@pytest.mark.django_db
def test_user_not_exist_returns_404_with_json(client, me_endpoint_url):
    """
    Test case: request user info for telegram_id not saved in database
    Should respond with 404 and JSON body with predefined structure, containing error message
    """
    request_url = me_endpoint_url(telegram_id=777)
    response = client.get(request_url)

    assert response.status_code == HTTPStatus.NOT_FOUND
    assert deserialize_json(response.content) == message_dict(text_lines_constants.rest_message_not_found)


@pytest.mark.skip(
    "Due to /me been rewritten in homework07 and there's no reason" " to fix it now according to homework08 roadmap"
)
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
def test_user_no_phone_number_set_returns_403_with_json_message(
    client: Client, me_endpoint_url, user_model_with_no_phone_number_set
):
    """
    Test case: request user info for telegram_id which have no phone number set in database
    Should respond with 403 and JSON body with predefined structure, containing error message
    """

    restricted_user = user_model_with_no_phone_number_set(telegram_id=123)
    restricted_user.save()

    request_url = me_endpoint_url(telegram_id=123)
    response = client.get(request_url)

    assert response.status_code == HTTPStatus.FORBIDDEN
    assert deserialize_json(response.content) == message_dict(
        app.internal.users.presentation.text_lines_constants.general_command_phone_number_not_set_error_message
    )


@pytest.mark.skip(
    "Due to /me been rewritten in homework07 and there's no reason" " to fix it now according to homework08 roadmap"
)
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
def test_user_with_valid_phone_number_set(client, me_endpoint_url, user_model_with_phone_number_set):
    """
    Test case: request user info for telegram id which have a valid phone number set in database
    Should respond with 200 and JSON-serialized TelegramBotUserInfo in body
    """

    verified_user = user_model_with_phone_number_set(telegram_id=123)
    verified_user.save()

    request_url = me_endpoint_url(telegram_id=123)
    response = client.get(request_url)

    user_info = TelegramBotUserInfo.from_orm(verified_user)

    assert response.status_code == HTTPStatus.OK
    assert deserialize_json(response.content) == user_info.dict()
