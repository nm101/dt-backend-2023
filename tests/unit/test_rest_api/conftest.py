import pytest

from app.internal.users.db.models import TelegramBotUser


@pytest.mark.skip()
@pytest.fixture
def user_model_with_no_phone_number_set():
    def inner(telegram_id: int) -> TelegramBotUser:
        return TelegramBotUser(telegram_id=telegram_id, username="test_user", phone_number=None)

    return inner


@pytest.mark.skip()
@pytest.fixture
def user_model_with_phone_number_set():
    def inner(telegram_id: int) -> TelegramBotUser:
        return TelegramBotUser(
            telegram_id=telegram_id,
            username="azino_777",
            phone_number="+78005553535",
        )

    return inner
