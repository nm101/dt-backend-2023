import pytest
from telegram import MessageEntity

import app.internal.bank_accounts.presentation.text_lines_constants
import app.internal.bank_cards.presentation.text_lines_constants
import app.internal.bank_transactions.presentation.text_lines_constants
import app.internal.users.presentation.text_lines_constants
from app.internal.common.domain.conversation_state import ConversationState


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_transfer_command_with_no_phone_number_set_replies_error_not_alters_state(
    mocked_ptb_context, ptb_message_update, ptb_user_registered_in_db, bot_commands_transaction
):
    mocked_update = ptb_message_update("/transfer", MessageEntity.BOT_COMMAND)

    state = await bot_commands_transaction.transfer(mocked_update, mocked_ptb_context)
    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        app.internal.users.presentation.text_lines_constants.general_command_phone_number_not_set_error_message,
    )

    assert state is None


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_transfer_command_with_valid_phone_number_set_replies_destination_options_alters_state(
    mocked_ptb_context, ptb_message_update, ptb_user_with_phone_number_set_in_db, bot_commands_transaction
):
    mocked_update = ptb_message_update("/transfer", MessageEntity.BOT_COMMAND)
    state = await bot_commands_transaction.transfer(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        app.internal.bank_transactions.presentation.text_lines_constants.command_transfer_specify_destination_type,
    )

    assert state == ConversationState.Transfer_AwaitingDestinationType


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_accounts_list_with_no_phone_number_set_replies_error(
    mocked_ptb_context, ptb_message_update, ptb_user_registered_in_db, bot_commands_accounts
):
    mocked_update = ptb_message_update("/list_accounts", MessageEntity.BOT_COMMAND)
    state = await bot_commands_accounts.accounts_list(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        app.internal.users.presentation.text_lines_constants.general_command_phone_number_not_set_error_message,
    )

    assert state is None


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_accounts_list_with_phone_number_no_accounts(
    mocked_ptb_context, ptb_message_update, ptb_user_with_phone_number_set_in_db, bot_commands_accounts
):
    mocked_update = ptb_message_update("/list_accounts", MessageEntity.BOT_COMMAND)
    state = await bot_commands_accounts.accounts_list(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        app.internal.bank_accounts.presentation.text_lines_constants.general_user_have_no_bank_accounts,
    )

    assert state is None


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_accounts_list_with_phone_number_with_account(
    mocked_ptb_context, ptb_message_update, ptb_user_with_bank_account_and_card, accounts_service, bot_commands_accounts
):
    mocked_update = ptb_message_update("/list_accounts", MessageEntity.BOT_COMMAND)
    accounts = await accounts_service.alist(mocked_update.message.from_user.id)

    state = await bot_commands_accounts.accounts_list(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        app.internal.bank_accounts.presentation.text_lines_constants.general_accounts_list(
            accounts, app.internal.bank_accounts.presentation.text_lines_constants.command_accounts_list_title
        ),
    )

    assert state is None


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_cards_list_with_no_phone_number_set_replies_error(
    mocked_ptb_context, ptb_message_update, ptb_user_registered_in_db, bot_commands_cards
):
    mocked_update = ptb_message_update("/list_cards", MessageEntity.BOT_COMMAND)
    state = await bot_commands_cards.cards_list(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        app.internal.users.presentation.text_lines_constants.general_command_phone_number_not_set_error_message,
    )

    assert state is None


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_cards_list_with_phone_number_no_cards(
    mocked_ptb_context, ptb_message_update, ptb_user_with_phone_number_set_in_db, bot_commands_cards
):
    mocked_update = ptb_message_update("/list_cards", MessageEntity.BOT_COMMAND)
    state = await bot_commands_cards.cards_list(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        app.internal.bank_cards.presentation.text_lines_constants.general_user_have_no_bank_cards,
    )

    assert state is None


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_cards_list_with_phone_number_with_card(
    mocked_ptb_context, ptb_message_update, ptb_user_with_bank_account_and_card, cards_service, bot_commands_cards
):
    mocked_update = ptb_message_update("/list_cards", MessageEntity.BOT_COMMAND)
    cards = await cards_service.alist(mocked_update.message.from_user.id)

    state = await bot_commands_cards.cards_list(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        app.internal.bank_cards.presentation.text_lines_constants.general_cards_list(
            cards, app.internal.bank_cards.presentation.text_lines_constants.command_cards_list_title
        ),
    )

    assert state is None
