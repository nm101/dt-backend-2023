import pytest
from telegram import MessageEntity

import app.internal.users.presentation.bot.states
import app.internal.users.presentation.bot.wrappers
import app.internal.users.presentation.text_lines_constants
from app.internal.bot import apply_wrappers
from app.internal.common.domain.bot_persistence_service import TelegramBotPersistenceService
from app.internal.common.domain.conversation_state import ConversationState
from app.internal.common.presentation import text_lines_constants
from app.internal.users.db.models import TelegramBotUser
from app.internal.users.domain.entities import TelegramBotUserInfo


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_start_command_handler_replies_correct_not_alters_state_saves_user(
    mocked_ptb_context, ptb_private_chat_with_user, ptb_message_update, bot_commands_users, user_service
):
    mocked_update = ptb_message_update("/start", MessageEntity.BOT_COMMAND)

    # because username is updated on every message by applying telegrambotuser_update_username_wrapper wrapper
    bot_wrappers = app.internal.users.presentation.bot.wrappers.BotWrappers(user_service)
    wrappers_active = (bot_wrappers.telegrambotuser_update_username_wrapper,)
    wrap_func = apply_wrappers(wrappers_active)

    handler = wrap_func(bot_commands_users.start)

    state = await handler(mocked_update, mocked_ptb_context)

    telegram_user = mocked_update.message.from_user

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        ptb_private_chat_with_user.id,
        app.internal.users.presentation.text_lines_constants.command_start_welcome_text(telegram_user),
    )

    assert state is None

    user: TelegramBotUser = await TelegramBotUser.objects.filter(telegram_id=telegram_user.id).afirst()

    assert user is not None
    assert user.telegram_id == telegram_user.id
    assert user.username == telegram_user.username


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_set_phone_state_valid_number_replies_success_alters_state_to_start(
    mocked_ptb_context, ptb_private_chat_with_user, ptb_message_update, bot_states_users
):
    mocked_update = ptb_message_update("+7 999 999 99 99", None)

    state = await bot_states_users.awaiting_phone_number(mocked_update, mocked_ptb_context)
    mocked_ptb_context.bot.send_message.assert_called_once_with(
        ptb_private_chat_with_user.id,
        app.internal.users.presentation.text_lines_constants.state_waiting_phone_number_success_text,
    )

    assert state == ConversationState.Start


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_me_command_with_no_phone_number_set_replies_error(
    mocked_ptb_context, ptb_private_chat_with_user, ptb_message_update, ptb_user_registered_in_db, bot_commands_users
):
    mocked_update = ptb_message_update("/me", MessageEntity.BOT_COMMAND)

    state = await bot_commands_users.me(mocked_update, mocked_ptb_context)
    mocked_ptb_context.bot.send_message.assert_called_once_with(
        ptb_private_chat_with_user.id,
        app.internal.users.presentation.text_lines_constants.general_command_phone_number_not_set_error_message,
    )

    assert state is None


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_me_command_with_valid_phone_number_set_replies_user_info(
    mocked_ptb_context,
    ptb_private_chat_with_user,
    ptb_message_update,
    ptb_user_with_phone_number_set_in_db,
    bot_commands_users,
):
    mocked_update = ptb_message_update("/me", MessageEntity.BOT_COMMAND)
    state = await bot_commands_users.me(mocked_update, mocked_ptb_context)

    telegram_id = ptb_user_with_phone_number_set_in_db.id
    user = await TelegramBotUser.objects.filter(telegram_id=telegram_id).aget()
    user_info = TelegramBotUserInfo.from_orm(user)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        ptb_private_chat_with_user.id,
        app.internal.users.presentation.text_lines_constants.command_me_user_information_text(user_info),
    )

    assert state is None


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_cancel_command_handler_with_phone_number_set_works(
    mocked_ptb_context,
    ptb_private_chat_with_user,
    ptb_message_update,
    ptb_user_with_phone_number_set_in_db,
    bot_commands_common,
    persistence_service,
):
    mocked_update = ptb_message_update("/cancel", MessageEntity.BOT_COMMAND)

    state = await bot_commands_common.cancel(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        ptb_private_chat_with_user.id, text_lines_constants.command_cancel_success_text
    )

    assert state == ConversationState.Start

    telegram_id = mocked_update.message.from_user.id
    with pytest.raises(KeyError):
        await persistence_service.aget_transaction_pending(telegram_id)


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_cancel_command_handler_with_phone_number_not_set_works(
    mocked_ptb_context, ptb_private_chat_with_user, ptb_message_update, ptb_user_registered_in_db, bot_commands_common
):
    mocked_update = ptb_message_update("/cancel", MessageEntity.BOT_COMMAND)

    state = await bot_commands_common.cancel(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        ptb_private_chat_with_user.id, text_lines_constants.command_cancel_success_text
    )

    assert state == ConversationState.Start
