import pytest
from telegram import MessageEntity

import app.internal.favourite_users_lists.presentation.text_lines_constants
import app.internal.users.presentation.text_lines_constants
from app.internal.common.domain.conversation_state import ConversationState
from app.internal.common.presentation import text_lines_constants


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_fave_list_with_no_phone_number_set_replies_error(
    mocked_ptb_context, ptb_message_update, ptb_user_registered_in_db, bot_commands_fave
):
    mocked_update = ptb_message_update("/fave_list", MessageEntity.BOT_COMMAND)
    state = await bot_commands_fave.fave_list(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        app.internal.users.presentation.text_lines_constants.general_command_phone_number_not_set_error_message,
    )

    assert state is None


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_fave_list_with_phone_number_set_list_empty(
    mocked_ptb_context, ptb_message_update, ptb_user_with_phone_number_set_in_db, bot_commands_fave
):
    mocked_update = ptb_message_update("/fave_list", MessageEntity.BOT_COMMAND)
    state = await bot_commands_fave.fave_list(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        app.internal.favourite_users_lists.presentation.text_lines_constants.general_fave_list_empty_message,
    )

    assert state is None


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_fave_list_with_phone_number_set_list_has_added_someone(
    mocked_ptb_context,
    ptb_message_update,
    ptb_user_with_phone_number_set_in_db,
    ptb_another_user_with_account,
    bot_commands_fave,
    favourites_service,
):
    another_user = await ptb_another_user_with_account(1234, "f40929f8-9537-43cf-bdcd-e4573c63270f", "0")

    await favourites_service.aadd(ptb_user_with_phone_number_set_in_db.id, another_user.id)
    favourites_list = await favourites_service.alist(ptb_user_with_phone_number_set_in_db.id)
    mocked_update = ptb_message_update("/fave_list", MessageEntity.BOT_COMMAND)

    state = await bot_commands_fave.fave_list(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        app.internal.favourite_users_lists.presentation.text_lines_constants.general_fave_list_list_format(
            favourites_list,
            title_text=app.internal.favourite_users_lists.presentation.text_lines_constants.command_fave_list_title,
        ),
    )

    assert state is None


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_fave_add_command_with_no_phone_number_set_replies_error(
    mocked_ptb_context, ptb_message_update, ptb_user_registered_in_db, bot_commands_fave
):
    mocked_update = ptb_message_update("/fave_add", MessageEntity.BOT_COMMAND)
    state = await bot_commands_fave.fave_add(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        app.internal.users.presentation.text_lines_constants.general_command_phone_number_not_set_error_message,
    )

    assert state is None


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_fave_add_command_with_valid_phone_number_set_replies_username_prompt_alters_state(
    mocked_ptb_context, ptb_message_update, ptb_user_with_phone_number_set_in_db, bot_commands_fave
):
    mocked_update = ptb_message_update("/fave_add", MessageEntity.BOT_COMMAND)
    state = await bot_commands_fave.fave_add(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        app.internal.users.presentation.text_lines_constants.general_username_specify_request_text,
    )

    assert state == ConversationState.FavoritesList_Add_AwaitingUsername


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_fave_add_awaiting_username_state_invalid_username_reply_error(
    mocked_ptb_context, ptb_message_update, ptb_user_with_phone_number_set_in_db, bot_states_fave
):
    mocked_update = ptb_message_update("definitelynonexistent", None)
    state = await bot_states_fave.favourites_list_add_awaiting_username(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        app.internal.users.presentation.text_lines_constants.general_user_not_found_text,
    )

    assert state is None


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_fave_add_awaiting_username_state_existent_username_reply_ok_alters_state_start(
    mocked_ptb_context,
    ptb_message_update,
    ptb_user_with_phone_number_set_in_db,
    ptb_another_user_with_account,
    bot_states_fave,
    user_service,
    favourites_service,
):
    another_user = await ptb_another_user_with_account(1234, "f40929f8-9537-43cf-bdcd-e4573c63270f", "0")
    another_user_info = await user_service.aget_by_username(another_user.username)

    mocked_update = ptb_message_update(another_user.username, None)
    state = await bot_states_fave.favourites_list_add_awaiting_username(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        app.internal.favourite_users_lists.presentation.text_lines_constants.state_favourite_list_add_awaiting_username_success_text,
    )

    assert state == ConversationState.Start

    fave_list = await favourites_service.alist(mocked_update.message.from_user.id)

    assert len(fave_list) == 1
    assert fave_list == (another_user_info,)


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_fave_add_awaiting_username_state_existent_username_but_startswith_at_sign_reply_ok_alters_state_start(
    mocked_ptb_context,
    ptb_message_update,
    ptb_user_with_phone_number_set_in_db,
    ptb_another_user_with_account,
    bot_states_fave,
    user_service,
    favourites_service,
):
    another_user = await ptb_another_user_with_account(1234, "f40929f8-9537-43cf-bdcd-e4573c63270f", "0")
    another_user_info = await user_service.aget_by_username(another_user.username)

    mocked_update = ptb_message_update(f"@{another_user.username}", None)
    state = await bot_states_fave.favourites_list_add_awaiting_username(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        app.internal.favourite_users_lists.presentation.text_lines_constants.state_favourite_list_add_awaiting_username_success_text,
    )

    assert state == ConversationState.Start

    fave_list = await favourites_service.alist(mocked_update.message.from_user.id)

    assert len(fave_list) == 1
    assert fave_list == (another_user_info,)


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_fave_del_command_with_no_phone_number_set_replies_error(
    mocked_ptb_context, ptb_message_update, ptb_user_registered_in_db, bot_commands_fave
):
    mocked_update = ptb_message_update("/fave_del", MessageEntity.BOT_COMMAND)
    state = await bot_commands_fave.fave_del(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        app.internal.users.presentation.text_lines_constants.general_command_phone_number_not_set_error_message,
    )

    assert state is None


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_fave_del_command_with_phone_number_set_list_empty(
    mocked_ptb_context, ptb_message_update, ptb_user_with_phone_number_set_in_db, bot_commands_fave
):
    mocked_update = ptb_message_update("/fave_del", MessageEntity.BOT_COMMAND)
    state = await bot_commands_fave.fave_del(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        app.internal.favourite_users_lists.presentation.text_lines_constants.general_fave_list_empty_message,
    )

    assert state is None


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_fave_del_command_with_phone_number_set_list_has_added_someone(
    mocked_ptb_context,
    ptb_message_update,
    ptb_user_with_phone_number_set_in_db,
    ptb_another_user_with_account,
    bot_commands_fave,
    favourites_service,
):
    another_user = await ptb_another_user_with_account(1234, "f40929f8-9537-43cf-bdcd-e4573c63270f", "0")

    await favourites_service.aadd(ptb_user_with_phone_number_set_in_db.id, another_user.id)
    favourites_list = await favourites_service.alist(ptb_user_with_phone_number_set_in_db.id)
    mocked_update = ptb_message_update("/fave_del", MessageEntity.BOT_COMMAND)

    state = await bot_commands_fave.fave_del(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        app.internal.favourite_users_lists.presentation.text_lines_constants.general_fave_list_list_format(
            favourites_list,
            app.internal.favourite_users_lists.presentation.text_lines_constants.command_fave_delete_title,
            is_numbered=True,
        ),
    )

    assert state == ConversationState.FavoritesList_Delete_AwaitingChoice


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_fave_del_awaiting_choice_incorrect_choice_replies_error(
    mocked_ptb_context,
    ptb_message_update,
    ptb_user_with_phone_number_set_in_db,
    ptb_another_user_with_account,
    bot_states_fave,
    favourites_service,
):
    another_user = await ptb_another_user_with_account(1234, "f40929f8-9537-43cf-bdcd-e4573c63270f", "0")

    await favourites_service.aadd(ptb_user_with_phone_number_set_in_db.id, another_user.id)
    favourites_list = await favourites_service.alist(ptb_user_with_phone_number_set_in_db.id)
    mocked_update = ptb_message_update("1337", None)

    state = await bot_states_fave.favourites_list_delete_awaiting_choice(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        text_lines_constants.general_incorrect_response_numeric_range_expected(1, len(favourites_list)),
    )

    assert state is None


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_fave_del_awaiting_choice_out_of_range_input_replies_error(
    mocked_ptb_context,
    ptb_message_update,
    ptb_user_with_phone_number_set_in_db,
    ptb_another_user_with_account,
    bot_states_fave,
    favourites_service,
):
    another_user = await ptb_another_user_with_account(1234, "f40929f8-9537-43cf-bdcd-e4573c63270f", "0")

    await favourites_service.aadd(ptb_user_with_phone_number_set_in_db.id, another_user.id)
    favourites_list = await favourites_service.alist(ptb_user_with_phone_number_set_in_db.id)
    mocked_update = ptb_message_update("two number 9s, a number 9 large, a number 6 with extra dip", None)

    state = await bot_states_fave.favourites_list_delete_awaiting_choice(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        text_lines_constants.general_incorrect_response_numeric_range_expected(1, len(favourites_list)),
    )

    assert state is None


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_fave_del_awaiting_choice_correct_choice_replies_success(
    mocked_ptb_context,
    ptb_message_update,
    ptb_user_with_phone_number_set_in_db,
    ptb_another_user_with_account,
    bot_states_fave,
    favourites_service,
):
    another_user = await ptb_another_user_with_account(1234, "f40929f8-9537-43cf-bdcd-e4573c63270f", "0")

    await favourites_service.aadd(ptb_user_with_phone_number_set_in_db.id, another_user.id)

    mocked_update = ptb_message_update("1", None)

    state = await bot_states_fave.favourites_list_delete_awaiting_choice(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        app.internal.favourite_users_lists.presentation.text_lines_constants.state_favourite_list_delete_success_text,
    )

    assert state == ConversationState.Start

    favourites_list = await favourites_service.alist(ptb_user_with_phone_number_set_in_db.id)
    assert len(favourites_list) == 0
