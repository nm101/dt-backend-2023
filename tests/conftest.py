import datetime
import json
from typing import Dict
from unittest.mock import AsyncMock

import pytest
from django.urls import reverse_lazy
from telegram import Chat, Message, MessageEntity, Update, User
from telegram.constants import MessageEntityType
from telegram.ext import Application, ApplicationBuilder

from app.internal.bank_accounts.db.repositories import BankAccountRepository, BankExchangeRateRepository
from app.internal.bank_accounts.domain.services import BankAccountService, BankExchangeRateService
from app.internal.bank_accounts.presentation.bot import commands as account_commands
from app.internal.bank_cards.db.repositories import BankCardRepository
from app.internal.bank_cards.domain.services import BankCardService
from app.internal.bank_cards.presentation.bot import commands as cards_commands
from app.internal.bank_transactions.domain.services import BankStatisticsService, BankTransactionService
from app.internal.bank_transactions.presentation.bot import (
    commands as transaction_commands,
    states as states_transactions,
)
from app.internal.bot import TelegramBot
from app.internal.common.domain.bot_persistence_service import TelegramBotPersistenceService
from app.internal.common.presentation.bot import commands as commands_common, states as states_common
from app.internal.favourite_users_lists.db.repositories import FavouriteUsersListsRepository
from app.internal.favourite_users_lists.domain.services import FavouriteUsersListsService
from app.internal.favourite_users_lists.presentation.bot import commands as commands_fave, states as states_fave
from app.internal.users.db.repositories import UserRepository
from app.internal.users.domain.services import UserService
from app.internal.users.presentation.bot import commands as commands_users, states as states_users
from app.models import TelegramBotUser


def deserialize_json(b: bytes):
    return json.loads(b)


def message_dict(message: str) -> Dict:
    return {"message": message}


@pytest.fixture
def me_endpoint_url():
    def inner_with_telegram_id(telegram_id):
        return reverse_lazy("api-1.0.0:me_endpoint", args=[telegram_id])

    return inner_with_telegram_id


@pytest.fixture
def ptb_user() -> User:
    return User(id=8675309, is_bot=False, first_name="boka", last_name="joka", username="bizarre_boka_joka_bros")


@pytest.fixture
def ptb_private_chat_with_user(ptb_user) -> Chat:
    return Chat(id=ptb_user.id, type="private")


@pytest.fixture
def ptb_user_registered_in_db(ptb_user) -> User:
    user_model = TelegramBotUser(telegram_id=ptb_user.id, username=ptb_user.username, phone_number=None)
    user_model.save()

    return ptb_user


@pytest.fixture
def ptb_user_with_phone_number_set_in_db(ptb_user_registered_in_db) -> User:
    user_model = TelegramBotUser.objects.filter(telegram_id=ptb_user_registered_in_db.id).first()
    user_model.phone_number = "+78005553535"
    user_model.save()

    return ptb_user_registered_in_db


@pytest.fixture
def mocked_ptb_context() -> AsyncMock:
    mocked_context = AsyncMock()
    mocked_context.bot.username = "testbot"

    return mocked_context


@pytest.fixture
def ptb_application(mocked_ptb_context) -> Application:
    application = ApplicationBuilder().bot(mocked_ptb_context.bot).updater(None).build()

    TelegramBot.setup_handlers(application)
    return application


@pytest.fixture
def ptb_message(ptb_user, ptb_private_chat_with_user, mocked_ptb_context):
    def inner(text: str, entity_type: MessageEntityType):
        custom_message = Message(
            message_id=123,
            date=datetime.datetime.now(),
            from_user=ptb_user,
            chat=ptb_private_chat_with_user,
            entities=(MessageEntity(length=len(text), offset=0, type=entity_type),),
            text=text,
        )

        custom_message._bot = mocked_ptb_context.bot
        return custom_message

    return inner


@pytest.fixture
def ptb_message_update(ptb_message, mocked_ptb_context):
    def inner(text: str, entity_type: MessageEntityType):
        message = ptb_message(text, entity_type)
        custom_update = Update(update_id=123, message=message)

        custom_update._bot = mocked_ptb_context.bot
        return custom_update

    return inner


@pytest.fixture
def user_service():
    user_repo = UserRepository()

    return UserService(user_repo)


@pytest.fixture
def accounts_service(user_service):
    account_repo = BankAccountRepository()
    return BankAccountService(account_repo, user_service)


@pytest.fixture
def cards_service(user_service):
    card_repo = BankCardRepository()
    return BankCardService(card_repo, user_service)


@pytest.fixture
def bot_commands_accounts(accounts_service):
    return account_commands.BotHandlers(accounts_service)


@pytest.fixture
def bot_commands_cards(cards_service):
    return cards_commands.BotHandlers(cards_service)


@pytest.fixture
def bot_commands_transaction(user_service, transaction_service, statistics_service):
    return transaction_commands.BotHandlers(user_service, transaction_service, statistics_service)


@pytest.fixture
def persistence_service(transaction_service, user_service):
    return TelegramBotPersistenceService(user_service, transaction_service)


@pytest.fixture
def favourites_service(user_service):
    fave_repo = FavouriteUsersListsRepository()
    return FavouriteUsersListsService(fave_repo, user_service)


@pytest.fixture
def exchange_rate_service():
    repo = BankExchangeRateRepository()
    return BankExchangeRateService(repo)


@pytest.fixture
def transaction_service(exchange_rate_service):
    return BankTransactionService(exchange_rate_service)


@pytest.fixture
def statistics_service(transaction_service):
    return BankStatisticsService(transaction_service)


@pytest.fixture
def bot_states_transaction(
    transaction_service,
    statistics_service,
    persistence_service,
    user_service,
    favourites_service,
    accounts_service,
    cards_service,
):
    return states_transactions.BotHandlers(
        transaction_service,
        statistics_service,
        persistence_service,
        user_service,
        favourites_service,
        accounts_service,
        cards_service,
    )


@pytest.fixture
def bot_commands_users(user_service):
    return commands_users.BotHandlers(user_service)


@pytest.fixture
def bot_states_users(user_service):
    return states_users.BotHandlers(user_service)


@pytest.fixture
def bot_commands_common(persistence_service):
    return commands_common.BotHandlers(persistence_service)


@pytest.fixture
def bot_states_common():
    return states_common.BotHandlers()


@pytest.fixture
def bot_commands_fave(user_service, favourites_service):
    return commands_fave.BotHandlers(user_service, favourites_service)


@pytest.fixture
def bot_states_fave(user_service, favourites_service):
    return states_fave.BotHandlers(user_service, favourites_service)
