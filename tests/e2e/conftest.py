import datetime
from decimal import Decimal

import pytest
from telegram import User

from app.internal.bank_accounts.db.models import BankAccount, BankCurrency
from app.internal.common.domain.conversation_state import ConversationState
from app.internal.common.presentation.bot.types import StateSettingCallback
from app.models import BankCard, TelegramBotUser


@pytest.fixture
def bank_currency() -> BankCurrency:
    currency = BankCurrency(name="fantiki")
    currency.save()
    return currency


@pytest.fixture
def bank_account(bank_currency):
    def inner(account_id: str, owner: TelegramBotUser, balance: Decimal) -> BankAccount:
        return BankAccount(id=account_id, owner=owner, currency=bank_currency, balance=balance)

    return inner


@pytest.fixture
def bank_card():
    def inner(number: int, account: BankAccount, owner: TelegramBotUser) -> BankCard:
        return BankCard(number=number, account=account, expiration_month=datetime.date(2004, 9, 10), owner=owner)

    return inner


@pytest.fixture
def ptb_user_with_bank_account_and_card(ptb_user_with_phone_number_set_in_db, bank_account, bank_card):
    user_model = TelegramBotUser.objects.filter(telegram_id=ptb_user_with_phone_number_set_in_db.id).first()
    new_account_model = bank_account(
        account_id="6eac95f7-b640-4ef5-a6cc-5a446252cb7e", owner=user_model, balance=Decimal("100500.49")
    )
    new_account_model.save()

    card = bank_card(number=1200300450067008, account=new_account_model, owner=user_model)
    card.save()

    return ptb_user_with_phone_number_set_in_db


@pytest.fixture
def user_model_with_valid_phone_number_set():
    def inner(telegram_id):
        return TelegramBotUser(
            telegram_id=telegram_id,
            username="ihavephone",
            phone_number="+78005553535",
        )

    return inner


@pytest.fixture
def ptb_another_user_with_account_and_card(user_model_with_valid_phone_number_set, bank_card, bank_account):
    async def inner(telegram_id, account_id, card_number, balance):
        new_user_model = user_model_with_valid_phone_number_set(telegram_id)
        await new_user_model.asave()

        new_account_model = bank_account(account_id, new_user_model, balance)
        await new_account_model.asave()

        new_card_model = bank_card(card_number, new_account_model, new_user_model)
        await new_card_model.asave()

        user = user_model_with_valid_phone_number_set(telegram_id)
        await user.asave()
        return User(id=telegram_id, is_bot=False, first_name=user.username, username=user.username)

    return inner


@pytest.fixture
def ptb_another_user_with_account(user_model_with_valid_phone_number_set, bank_account):
    async def inner(telegram_id, account_id, balance):
        new_user_model = user_model_with_valid_phone_number_set(telegram_id)
        await new_user_model.asave()

        new_account_model = bank_account(account_id, new_user_model, balance)
        await new_account_model.asave()

        user = user_model_with_valid_phone_number_set(telegram_id)

        return User(id=user.telegram_id, is_bot=False, first_name=user.username, username=user.username)

    return inner


async def process_request_reply(
    mocked_ptb_context,
    ptb_message_update,
    handler: StateSettingCallback,
    message_text: str,
    expected_response: str,
    expected_next_state: ConversationState,
):
    mocked_ptb_context.reset_mock()
    mocked_update = ptb_message_update(message_text, None)
    state = await handler(mocked_update, mocked_ptb_context)

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        mocked_update.message.from_user.id,
        expected_response,
    )
    assert state == expected_next_state
