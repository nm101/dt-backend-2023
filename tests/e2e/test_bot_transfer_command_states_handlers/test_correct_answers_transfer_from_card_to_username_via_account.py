from decimal import Decimal

import pytest

import app.internal.bank_accounts.presentation.text_lines_constants
import app.internal.bank_cards.presentation.text_lines_constants
import app.internal.bank_transactions.presentation.text_lines_constants
import app.internal.users.presentation.text_lines_constants
from app.internal.bank_accounts.domain.entities import BankAccountInfo
from app.internal.bank_cards.domain.entities import BankCardInfo
from app.internal.bank_transactions.db.models import BankTransaction
from app.internal.bank_transactions.domain.payment_type import PaymentType
from app.internal.common.domain.conversation_state import ConversationState
from app.internal.common.presentation import text_lines_constants
from tests.e2e.conftest import process_request_reply


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_full_cycle_correct_answers_transfer_from_card_to_username_via_account(
    mocked_ptb_context,
    ptb_message_update,
    ptb_user_with_bank_account_and_card,
    ptb_another_user_with_account_and_card,
    accounts_service,
    cards_service,
    persistence_service,
    bot_states_transaction,
):
    destination_account_id = "f40929f8-9537-43cf-bdcd-e4573c63270f"
    destination_card_number = 8676543212345678
    another_user = await ptb_another_user_with_account_and_card(
        1234, destination_account_id, destination_card_number, "0"
    )

    accounts = await accounts_service.alist(another_user.id)

    source_accounts = await accounts_service.alist(ptb_user_with_bank_account_and_card.id)
    source_account_id = source_accounts[0].id

    amount = Decimal("5432.11")

    # simulating destination type choice 2 - to user via username
    await process_request_reply(
        mocked_ptb_context,
        ptb_message_update,
        handler=bot_states_transaction.awaiting_destination_type,
        message_text="2",
        expected_response=app.internal.users.presentation.text_lines_constants.general_username_specify_request_text,
        expected_next_state=ConversationState.Transfer_ToParticularTelegramUser_AwaitingUsername,
    )

    # simulating username input
    await process_request_reply(
        mocked_ptb_context,
        ptb_message_update,
        handler=bot_states_transaction.to_particular_user_awaiting_username,
        message_text=another_user.username,
        expected_response=app.internal.bank_transactions.presentation.text_lines_constants.general_user_destination_type_text,
        expected_next_state=ConversationState.Transfer_ToParticularTelegramUser_AwaitingDestinationType,
    )

    transaction_pending: BankTransaction = await persistence_service.aget_transaction_pending(
        ptb_user_with_bank_account_and_card.id
    )
    assert transaction_pending.destination_user_id == another_user.id

    # simulating destination type choice 2 - to account
    await process_request_reply(
        mocked_ptb_context,
        ptb_message_update,
        handler=bot_states_transaction.to_particular_user_awaiting_dest_type,
        message_text="2",
        expected_response=app.internal.bank_accounts.presentation.text_lines_constants.general_accounts_list(
            accounts,
            app.internal.bank_transactions.presentation.text_lines_constants.transfer_to_user_accounts_list_title,
            displaying_to_owner=False,
            numbered=True,
        ),
        expected_next_state=ConversationState.Transfer_ToParticularTelegramUser_ToAccount_AwaitingChoice,
    )

    # simulating destination account choice - 1st and the only
    await process_request_reply(
        mocked_ptb_context,
        ptb_message_update,
        handler=bot_states_transaction.to_particular_user_to_account_awaiting_choice,
        message_text="1",
        expected_response=app.internal.bank_transactions.presentation.text_lines_constants.state_transfer_general_choose_source_type,
        expected_next_state=ConversationState.Transfer_AwaitingSourceType,
    )

    transaction_pending: BankTransaction = await persistence_service.aget_transaction_pending(
        ptb_user_with_bank_account_and_card.id
    )
    assert transaction_pending.destination_type == PaymentType.BankAccount
    assert str(transaction_pending.destination_account_id) == destination_account_id

    # simulating source type choice 1 - from bank card
    cards = await cards_service.alist(ptb_user_with_bank_account_and_card.id)

    await process_request_reply(
        mocked_ptb_context,
        ptb_message_update,
        handler=bot_states_transaction.awaiting_source_type,
        message_text="1",
        expected_response=(
            app.internal.bank_cards.presentation.text_lines_constants.general_cards_list(
                cards,
                title=app.internal.bank_transactions.presentation.text_lines_constants.state_transfer_source_type_card_list_title,
                numbered=True,
            )
            + "\n"
            + text_lines_constants.general_reply_only_with_digit_choice
        ),
        expected_next_state=ConversationState.Transfer_AwaitingSourceChoice,
    )

    transaction_pending: BankTransaction = await persistence_service.aget_transaction_pending(
        ptb_user_with_bank_account_and_card.id
    )
    assert transaction_pending.source_type == PaymentType.BankCard

    # simulating source card choice - 1st one (and the only) from list
    await process_request_reply(
        mocked_ptb_context,
        ptb_message_update,
        handler=bot_states_transaction.awaiting_source_choice,
        message_text="1",
        expected_response=app.internal.bank_transactions.presentation.text_lines_constants.state_transfer_awaiting_source_choice_amount_question,
        expected_next_state=ConversationState.Transfer_AwaitingAmount,
    )

    transaction_pending: BankTransaction = await persistence_service.aget_transaction_pending(
        ptb_user_with_bank_account_and_card.id
    )
    assert transaction_pending.source_card_id == cards[0].number
    assert transaction_pending.source_account_id == source_account_id

    # simulating amount typing
    await process_request_reply(
        mocked_ptb_context,
        ptb_message_update,
        handler=bot_states_transaction.awaiting_amount,
        message_text=str(amount),
        expected_response=app.internal.bank_transactions.presentation.text_lines_constants.text_prompt_request_attach_postcard,
        expected_next_state=ConversationState.Transfer_AwaitingChoiceAttachPostcard,
    )

    transaction_pending: BankTransaction = await persistence_service.aget_transaction_pending(
        ptb_user_with_bank_account_and_card.id
    )
    assert transaction_pending.amount == amount

    # simulating transaction confirmation

    source_info: BankCardInfo = await cards_service.aget(cards[0].number)
    destination_info: BankAccountInfo = await accounts_service.aget(destination_account_id)

    expected_balance_source = source_info.balance - amount
    expected_balance_destination = destination_info.balance + amount

    await process_request_reply(
        mocked_ptb_context,
        ptb_message_update,
        handler=bot_states_transaction.awaiting_confirmation,
        message_text="1",
        expected_response=app.internal.bank_transactions.presentation.text_lines_constants.state_transfer_awaiting_confirmation_transaction_success_message,
        expected_next_state=ConversationState.Start,
    )

    source_info: BankCardInfo = await cards_service.aget(cards[0].number)
    destination_info: BankAccountInfo = await accounts_service.aget(destination_account_id)

    # assert balance change
    assert source_info.balance == expected_balance_source
    assert destination_info.balance == expected_balance_destination
