from decimal import Decimal
from uuid import UUID

import pytest

import app.internal.bank_accounts.presentation.text_lines_constants
import app.internal.bank_transactions.presentation.text_lines_constants
from app.internal.bank_accounts.domain.entities import BankAccountInfo
from app.internal.bank_transactions.db.models import BankTransaction
from app.internal.bank_transactions.domain.payment_type import PaymentType
from app.internal.common.domain.conversation_state import ConversationState
from app.internal.common.presentation import text_lines_constants
from tests.e2e.conftest import process_request_reply


@pytest.mark.asyncio
@pytest.mark.unit
@pytest.mark.django_db(transaction=True)
async def test_full_cycle_correct_answers_transfer_from_account_to_account(
    mocked_ptb_context,
    ptb_message_update,
    ptb_user_with_bank_account_and_card,
    ptb_another_user_with_account,
    accounts_service,
    bot_states_transaction,
    persistence_service,
):
    destination_account_id = "f40929f8-9537-43cf-bdcd-e4573c63270f"
    await ptb_another_user_with_account(1234, destination_account_id, "0")

    source_accounts = await accounts_service.alist(ptb_user_with_bank_account_and_card.id)
    source_account_id = source_accounts[0].id

    amount = Decimal("5432.11")

    # simulating destination type choice 4 - to bank account
    await process_request_reply(
        mocked_ptb_context,
        ptb_message_update,
        handler=bot_states_transaction.awaiting_destination_type,
        message_text="4",
        expected_response=app.internal.bank_transactions.presentation.text_lines_constants.state_awaiting_destination_type_to_bank_account_message,
        expected_next_state=ConversationState.Transfer_ToBankAccount_AwaitingAccount,
    )

    # simulating account id input
    await process_request_reply(
        mocked_ptb_context,
        ptb_message_update,
        handler=bot_states_transaction.to_bank_account_awaiting_account,
        message_text=destination_account_id,
        expected_response=app.internal.bank_transactions.presentation.text_lines_constants.state_transfer_general_choose_source_type,
        expected_next_state=ConversationState.Transfer_AwaitingSourceType,
    )

    transaction_pending: BankTransaction = await persistence_service.aget_transaction_pending(
        ptb_user_with_bank_account_and_card.id
    )
    assert transaction_pending.destination_type == PaymentType.BankAccount
    assert transaction_pending.destination_account_id == UUID(destination_account_id)

    # simulating source type choice 2 - from bank account
    accounts = await accounts_service.alist(ptb_user_with_bank_account_and_card.id)

    await process_request_reply(
        mocked_ptb_context,
        ptb_message_update,
        handler=bot_states_transaction.awaiting_source_type,
        message_text="2",
        expected_response=(
            app.internal.bank_accounts.presentation.text_lines_constants.general_accounts_list(
                accounts,
                title=app.internal.bank_transactions.presentation.text_lines_constants.state_transfer_source_type_account_list_title,
                numbered=True,
            )
            + "\n"
            + text_lines_constants.general_reply_only_with_digit_choice
        ),
        expected_next_state=ConversationState.Transfer_AwaitingSourceChoice,
    )

    transaction_pending: BankTransaction = await persistence_service.aget_transaction_pending(
        ptb_user_with_bank_account_and_card.id
    )
    assert transaction_pending.source_type == PaymentType.BankAccount

    # simulating source account choice - 1st one (and the only) from list
    await process_request_reply(
        mocked_ptb_context,
        ptb_message_update,
        handler=bot_states_transaction.awaiting_source_choice,
        message_text="1",
        expected_response=app.internal.bank_transactions.presentation.text_lines_constants.state_transfer_awaiting_source_choice_amount_question,
        expected_next_state=ConversationState.Transfer_AwaitingAmount,
    )

    transaction_pending: BankTransaction = await persistence_service.aget_transaction_pending(
        ptb_user_with_bank_account_and_card.id
    )
    assert transaction_pending.source_account_id == source_account_id

    # simulating amount typing
    await process_request_reply(
        mocked_ptb_context,
        ptb_message_update,
        handler=bot_states_transaction.awaiting_amount,
        message_text=str(amount),
        expected_response=app.internal.bank_transactions.presentation.text_lines_constants.text_prompt_request_attach_postcard,
        expected_next_state=ConversationState.Transfer_AwaitingChoiceAttachPostcard,
    )

    transaction_pending: BankTransaction = await persistence_service.aget_transaction_pending(
        ptb_user_with_bank_account_and_card.id
    )
    assert transaction_pending.amount == amount

    # simulating transaction confirmation

    source_account_info: BankAccountInfo = await accounts_service.aget(source_account_id)
    destination_account_info: BankAccountInfo = await accounts_service.aget(destination_account_id)

    expected_balance_source = source_account_info.balance - amount
    expected_balance_destination = destination_account_info.balance + amount

    await process_request_reply(
        mocked_ptb_context,
        ptb_message_update,
        handler=bot_states_transaction.awaiting_confirmation,
        message_text="1",
        expected_response=app.internal.bank_transactions.presentation.text_lines_constants.state_transfer_awaiting_confirmation_transaction_success_message,
        expected_next_state=ConversationState.Start,
    )

    source_account_info: BankAccountInfo = await accounts_service.aget(source_account_id)
    destination_account_info: BankAccountInfo = await accounts_service.aget(destination_account_id)

    # assert balance change
    assert source_account_info.balance == expected_balance_source
    assert destination_account_info.balance == expected_balance_destination
