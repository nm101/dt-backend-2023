import pytest
from telegram import MessageEntity

from app.internal.users.presentation.text_lines_constants import command_start_welcome_text
from app.models import TelegramBotUser


@pytest.mark.smoke
@pytest.mark.asyncio
@pytest.mark.django_db
async def test_smoke(ptb_application, ptb_message_update, ptb_user, ptb_private_chat_with_user, mocked_ptb_context):
    """
    Test case:
    1) Starts PTB Application with actual bot handlers set
    2) Asserts the application is running
    2) Creates a mocked update for /start command, then sends it for processing to update queue
    3) Shuts down the application
    4) Makes assertions as follows:
        - A response for message was sent, and it's content complies with one defined in a service logic
        - A user, issued /start command was successfully and correctly saved to DB
    9) I’ll have two number 9s, a number 9 large, a number 6 with extra dip, a number 7,
        two number 45s, one with cheese, and a large soda.
        (kto ponyel tot ponyel, kto prochital s toi je intonaciey - meine respetung)
    """

    await ptb_application.initialize()
    await ptb_application.start()

    assert ptb_application.running

    mocked_update = ptb_message_update("/start", MessageEntity.BOT_COMMAND)
    await ptb_application.process_update(mocked_update)
    await ptb_application.stop()

    mocked_ptb_context.bot.send_message.assert_called_once_with(
        ptb_private_chat_with_user.id, command_start_welcome_text(ptb_user)
    )

    assert await TelegramBotUser.objects.acount() == 1

    user_model: TelegramBotUser = await TelegramBotUser.objects.filter(telegram_id=ptb_user.id).afirst()
    assert user_model is not None
    assert user_model.telegram_id == ptb_user.id
    assert user_model.username == ptb_user.username
