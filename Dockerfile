FROM python:3.10-slim-buster

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /app

# dynamic layer 1: linters, pytest config (for CI check_lint, docker_test) & Pipfile
COPY pyproject.toml setup.cfg .coveragerc pytest.ini Pipfile Pipfile.lock ./

# dynamic layer 2: pipenv installation & install dependencies
RUN pip --no-cache-dir install pipenv && \
    pipenv install --deploy --system && \
    pipenv --clear && \
    pip cache remove '*'

# dynamic layer 3: project sources & tests
COPY . /app/

# default cmd - bind gunicorn on all container ifaces at 8000/tcp
CMD ["gunicorn", "--chdir", "src/", "-b", "0.0.0.0:8000", "config.wsgi:application"]
