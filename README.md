# DT Backend Course 2023 homework
*by Melnikov Sergei, KN-301*

Contains homeworks implementation for DT backend course at 2023

As far as I know as the result it would be a sort of
online bank app with REST API and telegram bot interaction

## Docker deployment
### Compose services structure
There are 4 services utilized:
- nginx (internal reverse-proxy, exposes internal services as it should be & serves static files)
- app (app-runtime, starts gunicorn on 8000 TCP port)
- bot (app-runtime, starts telegram bot in a webhook mode on 8000 TCP port and /telegram URL)
- db (PostgreSQL DB)


### Quickstart steps (assuming make is installed):

First, fill .env file (see .env.example) 
to set Telegram bot secret key and change DB credentials

Then use these commands:
```
$ make up               # create and start services
$ make migrate          # apply migrations
$ make collectstatic    # collect static files
$ make createsuperuser  # create django superuser
```

#### Other (probably) useful docker make commands
To build runtime image beforehand (compose up builds image automatically if not present):
```
$ make build
```

To access command shell in running services containers:
```
$ make docker_shell_app   # app container
$ make docker_shell_bot   # bot container
$ make docker_shell_db    # db container
$ make docker_shell_nginx # nginx container
```

## Branches structure and code reviews rules
*That part exists just for myself to keep things stated and clearly defined*

*Might be changed depending on mentors advices & course rules in general
(I don't know anything about actual course interaction format 
and what of stated below would actually be useful 
and what would not, so let it be here at least as a placeholder)*

- Each homework should be implemented in its own separate branch
 (ex: homework01, homework02, etc., clear naming convention should be kept) 
  regardless of its feature set complexity.

  However, some large features might be implemented in separate branches
  and then merged into corresponding homework branch 

- After homework completion, an MR to the master branch should be open

- During homework submission **all** problematic lines **should** be commented in corresponding,
in general, separate threads and then resolved as the problem solved

*Consider problem as solved if nobody mentioned that code segment again during the next submissions, 
otherwise thread **must** be re-opened with the new feedback stated*

*If the mentors team would not appreciate that code review documentation rules set, which 
I partially stole (as my memory managed to keep subjectively the most important things)
from scala course and wouldn't play along (make text comments in threads during the code reviews),
I shall either adapt to a new suggested one, or shall maintain this one completely on my own
depending on circumstances*

- Hope I'll manage to keep things structured and in the worst case scenario 
would be able to perceive advices 
and document 'em if not as was planned above then any other proper way
