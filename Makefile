# CI variables defaults for local deployment
DEFAULTS_CI_REGISTRY := registry.local
DEFAULTS_CI_ENVIRONMENT_SLUG := local
DEFAULTS_CI_COMMIT_SHA := none
# try import variables before env evaluation
-include .env_make

#  set docker compose file overrides:
#   dev  - if CI_ENVIRONMENT_SLUG is default
#   prod - otherwise
ifeq ($(CI_ENVIRONMENT_SLUG),$(DEFAULTS_CI_ENVIRONMENT_SLUG))
	COMPOSE_OVERRIDE_SLUG := dev
else
	COMPOSE_OVERRIDE_SLUG := prod
endif
export COMPOSE_PATH_SEPARATOR ?= :
export COMPOSE_FILE ?= docker-compose.yml:docker-compose.${COMPOSE_OVERRIDE_SLUG}.yml

#  if CI variables unfilled -> not running by gitlab CI -> need to set something
export CI_REGISTRY ?= $(DEFAULTS_CI_REGISTRY)
export CI_ENVIRONMENT_SLUG ?= $(DEFAULTS_CI_ENVIRONMENT_SLUG)
export CI_COMMIT_SHA ?= $(DEFAULTS_CI_COMMIT_SHA)

# commands, that should be callable both locally and inside container
COMMANDS_CHECK_LINT := isort --check --diff . && \
	flake8 --config setup.cfg && \
	black --check --config pyproject.toml .

COMMANDS_CHECK_MAKEMIGRATIONS := python src/manage.py makemigrations --check --dry-run

COMMANDS_TEST := pytest --cov-reset --cov=. --no-cov-on-fail tests/

# development commands (could be ran locally)
env_info:
	@echo "Environment info:"
	@echo "  CI_REGISTRY         : '$${CI_REGISTRY}'"
	@echo "  CI_ENVIRONMENT_SLUG : '$${CI_ENVIRONMENT_SLUG}'"
	@echo "  CI_COMMIT_SHA       : '$${CI_COMMIT_SHA}'"
	@echo "  COMPOSE_FILE        : '$${COMPOSE_FILE}'"

piplock:
	pipenv install
	sudo chown -R ${USER} Pipfile.lock

env_shell:
	pipenv shell

env_bash:
	bash

shell:
	docker compose run --rm -it app python src/manage.py shell

lint:
	isort .
	flake8 --config setup.cfg
	black --config pyproject.toml .

check_lint:
	bash -c "${COMMANDS_CHECK_LINT}"

test:
	${COMMANDS_TEST}

check: check_lint check_makemigrations test

makemigrations:
	python src/manage.py makemigrations
	sudo chown -R ${USER} src/app/migrations/

check_makemigrations:
	${COMMANDS_CHECK_MAKEMIGRATIONS}

tank:
	docker run --rm -v ./load.yaml:/var/loadtest/load.yaml -v ./token.txt:/var/loadtest/token.txt -it yandex/yandex-tank

# deployment management commands (could be ran locally)

migrate:
	docker compose run --rm app bash -c 'python src/manage.py migrate $(if $m, api $m,)'

createsuperuser:
	docker compose run --rm -it app python src/manage.py createsuperuser

collectstatic:
	docker compose run --rm --no-deps app python src/manage.py collectstatic --no-input

build:
	docker compose build

push:
	docker compose push

pull:
	docker compose pull

docker_login:
	docker login -u $$CI_REGISTRY_USER -p $$CI_REGISTRY_PASSWORD $$CI_REGISTRY

up:
	docker compose up -d

up_verbose:
	docker compose up

down:
	docker compose down

down_erase_volumes:
	docker compose down -v

logs_app:
	docker compose logs app

logs_bot:
	docker compose logs bot

ps:
	docker compose ps -a

docker_check_lint:
	docker compose run --rm --no-deps app bash -c "${COMMANDS_CHECK_LINT}"

docker_check_makemigrations:
	docker compose run --rm --no-deps app ${COMMANDS_CHECK_MAKEMIGRATIONS}

docker_test:
	docker compose run --rm --no-deps app ${COMMANDS_TEST}

docker_shell_app:
	docker compose exec -it app bash

docker_shell_bot:
	docker compose exec -it bot bash

docker_shell_db:
	docker compose exec -it db bash

docker_shell_nginx:
	docker compose exec -it nginx ash
